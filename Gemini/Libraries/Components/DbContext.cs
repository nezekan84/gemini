﻿using System;
using System.Data.Entity.Validation;
using Gemini.Models;

namespace Gemini.Libraries.Components
{
    /// <summary>
    /// Since: Rev 5a
    /// Current: Rev 11a
    /// </summary>
    public abstract class DbContext : IDisposable
    {
        protected ApplicationDbContext Context { get; set; }

        public DbContext()
        {
            Context = new ApplicationDbContext();
        }

        public void Dispose()
        {
            Context.Dispose();
        }

        public int Save()
        {
            try
            {
                return Context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                throw e;
            }
        }
    }
}