﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace Gemini.Libraries.Components.Toast
{
    /// <summary>
    /// Since: Rev 3
    /// Current:  Rev 3
    /// </summary>
    public static class NotifyHelper
    {
        public static IHtmlString Notify(this HtmlHelper helper, Notify notify)
        {
            TagBuilder alertBuilder = new TagBuilder("input");
            alertBuilder.Attributes.Add("type", "hidden");
            alertBuilder.Attributes.Add("id", "Notify_Alert");
            alertBuilder.Attributes.Add("value", notify.Alert);

            TagBuilder durationBuilder = new TagBuilder("input");
            durationBuilder.Attributes.Add("type", "hidden");
            durationBuilder.Attributes.Add("id", "Notify_Duration");
            durationBuilder.Attributes.Add("value", notify.Duration.ToString());

            TagBuilder iconBuilder = new TagBuilder("input");
            iconBuilder.Attributes.Add("type", "hidden");
            iconBuilder.Attributes.Add("id", "Notify_Icon");
            iconBuilder.Attributes.Add("value", notify.Icon);

            TagBuilder messageBuilder = new TagBuilder("input");
            messageBuilder.Attributes.Add("type", "hidden");
            messageBuilder.Attributes.Add("id", "Notify_Message");
            messageBuilder.Attributes.Add("value", notify.Message);

            TagBuilder showBuilder = new TagBuilder("input");
            showBuilder.Attributes.Add("type", "hidden");
            showBuilder.Attributes.Add("id", "Notify_Show");
            showBuilder.Attributes.Add("value", notify.Show.ToString());

//            to be used for multiple notifs.
//            TagBuilder stackingBuilder = new TagBuilder("input");
//            stackingBuilder.Attributes.Add("type", "hidden");
//            stackingBuilder.Attributes.Add("id", "Notify_Stacking");
//            stackingBuilder.Attributes.Add("value", notify.Stacking.ToString());

            TagBuilder titleBuilder = new TagBuilder("input");
            titleBuilder.Attributes.Add("type", "hidden");
            titleBuilder.Attributes.Add("id", "Notify_Title");
            titleBuilder.Attributes.Add("value", notify.Title);

            TagBuilder toggleBuilder = new TagBuilder("input");
            toggleBuilder.Attributes.Add("type", "hidden");
            toggleBuilder.Attributes.Add("id", "Notify_Toggle");
            toggleBuilder.Attributes.Add("value", notify.Toggle.ToString());

            TagBuilder typeBuilder = new TagBuilder("input");
            typeBuilder.Attributes.Add("type", "hidden");
            typeBuilder.Attributes.Add("id", "Notify_Type");
            typeBuilder.Attributes.Add("value", notify.Type);

            return new MvcHtmlString($"{alertBuilder}{durationBuilder}{iconBuilder}{messageBuilder}{showBuilder}{titleBuilder}{toggleBuilder}{typeBuilder}");
        }

        // TO DO: add support for multiple notifications.
        public static IHtmlString Notify(this HtmlHelper helper, List<Notify> notifs)
        {
            throw new NotImplementedException();
        }
    }
}