﻿namespace Gemini.Libraries.Components.Toast
{
    /// <summary>
    /// Since: Rev 3
    /// Current: Rev 4
    /// 
    /// referrence: https://www.npmjs.com/package/cheers-alert
    /// </summary>
    public class Notify
    {
        public string Alert { get; set; } = AlertSlide;

        public byte Duration { get; set; } = 3;

        public string Icon { get; set; } = "fa-exclamation";

        public string Message { get; set; } = "Placehoder.";

        public bool Show { get; set; } = false;

        public bool Stacking { get; set; } = true;

        public string Title { get; set; } = "Placeholder.";

        public bool Toggle { get; set; } = true;

        public string Type { get; set; } = TypeInfo;

        public const string AlertSlide = "slideleft";
        public const string AlertFade = "fadein";

        public const string TypeSuccess = "success";
        public const string TypeWarning = "warning";
        public const string TypeError = "error";
        public const string TypeInfo = "info";
    }
}