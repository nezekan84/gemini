﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;

namespace Gemini.Libraries.Components.Settings
{
    /// <summary>
    /// Since: Rev 5a
    /// Current: Rev 22a
    /// </summary>
    public class SettingsComponent : DbContext
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public string Get(string entity, string attribute)
        {
            Models.Settings model = Context.Settings.SingleOrDefault(x => x.Entity == entity && x.Attribute == attribute);

            return model?.Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="attribute"></param>
        /// <param name="value"></param>
        /// <param name="forceUpdate"></param>
        /// <returns></returns>
        public int Set(string entity, string attribute, string value, bool forceUpdate = false)
        {
            bool unique = IsUnique(entity, attribute);
            // if entity attribute pair is unique then create it.
            if (unique)
            {
                Models.Settings settings = new Models.Settings()
                {
                    UserId = HttpContext.Current.User.Identity.GetUserId(),
                    Entity = entity,
                    Attribute = attribute,
                    Value = value,
                    DateCreated = DateTime.Now
                };
                
                Context.Settings.Add(settings);
            }

            // this part assumes that entity attribute pair is not unique and
            // therefore will update the pair's value if forceUpdate is true
            if (forceUpdate && !unique)
            {
                Models.Settings settings = Context.Settings.SingleOrDefault(x => x.Entity == entity && x.Attribute == attribute);
                settings.Value = value;
                Context.Entry(settings).State = EntityState.Modified;
            }

            int save = Save();
            return save;
        }

        /// <summary>
        /// Checks if Entity-Attribute pair is unique
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public bool IsUnique(string entity, string attribute)
        {
            return Context.Settings.SingleOrDefault(x => x.Entity == entity && x.Attribute == attribute) == null;
        }
    }
}