﻿module.exports = function (grunt) {
    grunt.initConfig({
        embedFonts: {
            all: {
                files: {
                    "Content/fonts.css": [
                        "node_modules/sh-core/bin/main.css",
                        "node_modules/sh-icons/bin/main.css",
                        "node_modules/sh-buttons/bin/main.css"
                    ]
                }
            }
        },
        concat: {
            css: {
                src: [
                    "Content/bootstrap.css",
                    "css/font-awesome.css",
                    "admin-lte/css/AdminLTE.css",
                    "admin-lte/css/skins/_all-skins.css",
                    "node_modules/cheers-alert/dist/cheers-alert.min.css",
                    "node_modules/react-confirm-alert/src/react-confirm-alert.css"
                ],
                dest: "Content/app.css"
            }
        },
        cssmin: {
            'options': {
                'processImport': false
            },
            css: {
                src: "Content/app.css",
                dest: "Content/app.min.css"
            }
        },
        uglify: {
            main: {
                files: {
                    "Content/main.js": [
                        "Scripts/jquery-3.1.1.min.js",
                        "Scripts/jquery.validate*",
                        "Scripts/bootstrap.min.js"
                    ]
                }
            },
            extensions: {
                files: {
                    "Content/extensions.js": [
                        "Scripts/modernizr-*",
                        "Scripts/respond.js",
                        "admin-lte/js/app.js"
                    ]
                }
            },
            commonCombine: {
                files: {
                    "Content/app.js": [
                        "Content/main.js",
                        "Content/extensions.js"
                    ]
                }
            }
        }
    });

    // Load the plugin that provides the tasks we need
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks("grunt-contrib-cssmin");
    grunt.loadNpmTasks("grunt-embed-fonts");

    // Default task(s).
    grunt.registerTask("default", []);

    // Build task(s).
    grunt.registerTask("build:css", ["concat:css", "cssmin:css", "embedFonts", "uglify"]);
};