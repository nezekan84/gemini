﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.Enums;
using Gemini.Models;

namespace Gemini.Libraries.Models
{
    /// <summary>
    /// Since: Rev ?a
    /// Current: Rev 4
    /// </summary>
    public class Posts
    {
        [Key]
        public int PostId { get; set; }

        public string UserId { get; set; }

        public ApplicationUser User { get; set; }

        public string Title { get; set; }

        public string Slug { get; set; }

        public string Content { get; set; }

        public PostTypes PostType { get; set; }

        public PostStatus PostStatus { get; set; }

        public ICollection<Posts> Children { get; set; }

        public int? ParentId { get; set; }

        public Posts Parent { get; set; }

        public ICollection<PostMeta> Meta { get; set; }

        public ICollection<PostGroup> Group { get; set; }

        public PostVisibility Visibility { get; set; }

        public DateTime DateUpdated { get; set; }

        public DateTime DateCreated { get; set; }
    }
}