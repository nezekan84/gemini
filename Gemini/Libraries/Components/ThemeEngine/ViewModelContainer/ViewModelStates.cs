﻿using System;
using System.Collections.Generic;

namespace Gemini.Libraries.Components.ThemeEngine.ViewModelContainer
{
    /// <summary>
    /// Since: Rev 4
    /// Current: Rev 5
    /// </summary>
    public class ViewModelStates
    {
        private List<IViewModelState> Container { get; } = new List<IViewModelState>();

        public bool Exists(IViewModelState state)
        {
            return Container.Exists(x => x.GetType() == state.GetType());
        }

        public IViewModelState Get(IViewModelState state)
        {
            if (!Exists(state)) throw new Exception();

            return Container.Find(x => x.GetType() == state.GetType());
        }

        public void Push(IViewModelState state)
        {
            Container.Remove(state);
            Container.Add(state);
        }
    }
}