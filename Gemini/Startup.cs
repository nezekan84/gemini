﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Gemini.Startup))]
namespace Gemini
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
