﻿using System.Collections.Generic;

namespace Gemini.ViewModels.Categories
{
    public class CategoryDeleteListViewModel
    {
        public List<CategoryDeleteItemViewModel> Items { get; set; }
    }
}