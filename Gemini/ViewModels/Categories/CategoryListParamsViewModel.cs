﻿using System.Collections.Generic;

namespace Gemini.ViewModels.Categories
{
    public class CategoryListParamsViewModel
    {
        public string FormAction { get; set; }

        public string TableUrl { get; set; }

        public string ModalDataUrl { get; set; }

        public List<CategoryViewModel> Categories { get; set; }

        public string UpdateAction { get; set; }

        public string DeleteAction { get; set; }
    }
}