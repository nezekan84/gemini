﻿using System.Collections.Generic;
using Gemini.Libraries.Models;

namespace Gemini.ViewModels.Categories
{
    public class CategoryListViewModel
    {
        public IEnumerable<CategoryViewModel> Categories { get; set; }
    }
}