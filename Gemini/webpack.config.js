module.exports = {
    entry: {
        gemini: __dirname + "/Scripts/gemini.js",
        cat: __dirname + "/app/Categories/list.jsx",
        tagList: __dirname + "/app/Tags/taglist.jsx",
        generalForm: __dirname + "/app/Settings/generalForm.jsx",
        notify: __dirname + "/app/Shared/notify.jsx"
    },
	output: {
		path: __dirname + "/app",
		filename: "[name].bundle.js",
	},
	resolve: {
		extensions: [".js", ".jsx"]
	},
	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				loader: "babel-loader",
				exclude: /node_modules/,
				query: {
					presets: ["react", "es2015"]
				}
			}
		]
	}
}