﻿using System.Data.Entity.ModelConfiguration;

namespace Gemini.Libraries.Models.Configurations
{
    /// <summary>
    /// Since: Rev 7a
    /// Current: Rev 11a
    /// </summary>
    public class SettingsConfig : EntityTypeConfiguration<Settings>
    {
        public SettingsConfig()
        {
            ToTable("Settings");

            // relationships
            HasRequired(settings => settings.User)
                .WithMany(user => user.Settings)
                .HasForeignKey(settings => settings.UserId)
                .WillCascadeOnDelete(false);
        }
    }
}