﻿using System;
using System.Web;
using Gemini.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Gemini.Libraries.Components
{
    /// <summary>
    /// Since: Rev 11a
    /// Current: Rev 11a
    /// </summary>
    public class Auth
    {
        public ApplicationUser Identity { get; set; }

        protected UserManager<ApplicationUser> Manager { get; set; }

        [ThreadStatic]
        private static Auth _instance;

        /// <summary>
        /// Get instance
        /// </summary>
        public static Auth Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Auth();
                    _instance.Initialize();
                }

                return _instance;
            }
        }

        /// <summary>
        /// Initialize Auth
        /// </summary>
        public void Initialize()
        {
            Manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));

            if (IsAuth())
            {
                var uid = HttpContext.Current.User.Identity.GetUserId();
                Instance.Identity = Manager.FindById(uid);
            }
        }

        /// <summary>
        /// CHeck if user is authenticated
        /// </summary>
        /// <returns></returns>
        public static bool IsAuth()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated;
        }

        public string ShowName()
        {
            if (IsAuth() && Instance.Identity != null)
            {
                return _instance.Identity.Email;
            }

            return null;
        }
    }
}