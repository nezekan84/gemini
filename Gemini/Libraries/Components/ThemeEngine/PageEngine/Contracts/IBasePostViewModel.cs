﻿using System;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.Enums;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.Factory;
using Gemini.Libraries.Components.ThemeEngine.ViewModelContainer;
using Gemini.Models;

namespace Gemini.Libraries.Components.ThemeEngine.PageEngine.Contracts
{
    /// <summary>
    /// Since: Rev 5a
    /// Current: Rev 5
    /// </summary>
    public interface IBasePostViewModel : IViewModelFactoryItem, IViewModelState
    {
        int Id { get; set; }
        
        string UserId { get; set; }

        ApplicationUser User { get; set; }

        PostVisibility Visibility { get; set; }

        PostStatus Status { get; set; }

        string Title { get; set; }

        string Content { get; set; }

        PostTypes Type { get; set; }

        int ParentId { get; set; }

        DateTime DateUpdated { get; set; }

        DateTime DateCreated { get; set; }

        string ModelType { get; set; }
    }
}
