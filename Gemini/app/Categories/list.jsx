﻿import React from 'react';
import ReactDOM from "react-dom";
import {
    Modal as ReactModal,
    ModalHeader as ReactModalHeader,
    ModalTitle as ReactModalTitle,
    ModalBody as ReactModalBody,
    ModalFooter as ReactModalFooter,
    Button as ReactBtn,
    OverlayTrigger
    } from "react-bootstrap";
import Cheers from 'cheers-alert';
import { confirmAlert } from 'react-confirm-alert';

class ModalErrorRow extends React.Component {
    render() {
        return <li>{this.props.message}</li>;
    }
}

class ModalErrors extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
             errors: this.props.errors
        };
    }
    render() {
        return (
             <ul>
             {this.state.errors.map(function(i) {
                 return <ModalErrorRow message={i} key={Date.now()} />;
             })}
            </ul>
        );
    }
}

class CategoryComposer extends React.Component {
    constructor(props) {
        super(props);

        var tdName = "";
        if (this.props.cat.ParentId === null) {
            tdName = this.props.cat.Name;
        } else {
            tdName = `- ${this.props.cat.Name}`;
        }
        this.state = { Name: tdName, PostCount: this.props.cat.PostCount };

        this.onChange = this.onChange.bind(this);
        this.proceedDelete = this.proceedDelete.bind(this);
    }
    onChange(event) {
        event.persist();
        let checked = event.target.checked;
        if (this.props.cat.ParentId === null && checked) {
            confirmAlert({
                title: "Warning",
                message: "You are attempting to delete a parent category. Are you sure you want to continue?",
                confirmLabel: "Confirm",
                cancelLabel: "Cancel",
                onConfirm: () => this.proceedDelete(checked),
                onCancel: () => event.target.checked = false
            });
        } else {
            this.proceedDelete(checked);
        }
    }
    proceedDelete(checked) {
        if (!checked) {
            this.props.uncheckCategory(this.props.cat.Id);
        } else {
            this.props.checkCategory(this.props.cat.Id);
        }
    }
    render() {
        return (
            <tr>
                <td>
                    <input type="checkbox" onChange={this.onChange} checked={this.props.isCatChecked(this.props.cat.Id)}/>
                </td>
                <td onClick={this.props.setEditMode.bind(null, this.props.cat)}>{this.state.Name}</td>
                <td>{this.state.PostCount}</td>
            </tr>
        );
    }
}

class CategoryTbl extends React.Component {
    constructor(props) {
        super(props);

        this.onDelete = this.onDelete.bind(this);
    }
    onDelete() { 
        if (this.props.queueDelete().length > 0) {
            $.ajax({
                context:this,
                url: document.getElementById("DeleteAction").value,
                method: "post",
                data: {
                    __RequestVerificationToken: document.querySelector("[name='__RequestVerificationToken']").value,
                    Items: this.props.queueDelete()
                },
                success: function(response) {
                    if (response.Errors) {
                        Cheers.warning({
                            title: "An Error Has Occurred!",
                            message: response.Message,
                            alert: "slideleft",
                            icon: "fa-times",
                            duration: 3
                        });
                    } else {
                        this.props.refreshList();
                        this.props.resetQueueDelete();
                        Cheers.info({
                            title: "Updated!",
                            message: "Category list updated.",
                            alert: "slideleft",
                            icon: "fa-exclamation",
                            duration: 3
                        });
                    }
                    this.props.unsetEditMode();
                }
            });
        }
    }
    render() {
        return (
            <table className="table table-hover">
                <thead>
                     <tr>
                         <th>
                             <input id="chk-delall-top" type="checkbox" onChange={this.props.allCatSelector} />
                             <button className="btn btn-primary" onClick={this.onDelete}>Delete</button>
                         </th>
                         <th>Name</th>
                         <th>Post Count</th>
                     </tr>
                </thead>
                <tbody>
                    {this.props.categories.map(function(data) {
                        return <CategoryComposer 
                            cat={data} 
                            key={data.Name + this.props.isCatChecked(data.Id).toString()} 
                            setEditMode={this.props.setEditMode} 
                            unsetEditMode={this.props.unsetEditMode}
                            checkCategory={this.props.checkCategory}
                            uncheckCategory={this.props.uncheckCategory}
                            isCatChecked={this.props.isCatChecked} />;
                    }, this)}
                </tbody>
                <tfoot>
                    <tr>
                         <th>
                             <input id="chk-delall-bot" type="checkbox" onChange={this.props.allCatSelector} />
                             <button className="btn btn-primary" onClick={this.onDelete}>Delete</button>
                         </th>
                         <th>Name</th>
                         <th>Post Count</th>
                     </tr>
                </tfoot>
            </table>
        );
    }
}

class CategoryContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            description: "",
            name: "",
            parent: 0,
            errors: [],
            show: false
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.onNameChange = this.onNameChange.bind(this);
        this.onParentChange = this.onParentChange.bind(this);
        this.onDescChange = this.onDescChange.bind(this);
        this.onHide = this.onHide.bind(this);
        this.onShow = this.onShow.bind(this);
    }
    onSubmit(e) {
        e.preventDefault();
        $.ajax({
            context: this,
            url: document.getElementById("FormAction").value,
            method: "post",
            data: {
                __RequestVerificationToken: document.querySelector("[name='__RequestVerificationToken']").value,
                Name: this.state.name,
                ParentId: this.state.parent,
                Description: this.state.description
            },
            success: function (response) {
                if (response.Errors === true) {
                    var errs = response.ErrCol;
                    this.setState({ errors: errs });
                } else {
                    this.props.refreshList();
                    this.onHide();
                    this.notify();
                }
            }
        });
    }
    notify() {
        Cheers.success({
            title: "Category Created!",
            message: "You have created a new category.",
            alert: "slideleft",
            icon: "fa-check",
            duration: 3
        });
    }
    onNameChange(e) {
        this.setState({ name: e.target.value });
    }
    onParentChange(e) {
        this.setState({ parent: e.target.value });
    }
    onDescChange(e) {
        this.setState({ description: e.target.value });
    }
    onHide() {
        this.setState({
            show: false,
            description: "",
            name: "",
            parent: 0,
            errors: []
        });
    }
    onShow() {
        this.setState({ show: true });
    }
    render() {
        var errs = null;
        if(this.state.errors.length > 0) {
            errs = <ModalErrors errors={this.state.errors}/>;
        }
        return (
            <div className="box box-primary">
                <div className="box-header">
                    <h3 className="box-title">Categories</h3>
                    <ReactBtn bsStyle="primary" className="pull-right" onClick={this.onShow}>Add New</ReactBtn>
                </div>
                <div className="box-body table-responsive">
                    <CategoryTbl 
                        categories={this.props.categories} 
                        setEditMode={this.props.setEditMode} 
                        unsetEditMode={this.props.unsetEditMode}
                        checkCategory={this.props.checkCategory}
                        uncheckCategory={this.props.uncheckCategory}
                        isCatChecked={this.props.isCatChecked}
                        allCatSelector={this.props.allCatSelector}
                        queueDelete={this.props.queueDelete}
                        resetQueueDelete={this.props.resetQueueDelete}
                        refreshList={this.props.refreshList}/>
                </div>
                <ReactModal id="new-catg-modal" show={this.state.show} onHide={this.onHide}>
                    <form className="form-horizontal" method="post" onSubmit={this.onSubmit}>
                        <ReactModalHeader closeButton>
                            <ReactModalTitle>New Category</ReactModalTitle>
                        </ReactModalHeader>
                        <ReactModalBody>
                            {errs}
                            <div className="form-group">
                                <label className="control-label col-md-2" htmlFor="cat-name">Name</label>
                                <div className="col-md-10">
                                    <input className="form-control" id="cat-name" name="Name" type="text" onChange={this.onNameChange} value={this.state.name} />
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="control-label col-md-2" htmlFor="cat-parent">Parent</label>
                                <div className="col-md-10">
                                    <select className="form-control" id="cat-parent" name="ParentId" onChange={this.onParentChange}>
                                        {this.props.modalOpts.map(function(cat) {
                                            return <option value={cat.Id} key={cat.Id}>{cat.Name}</option>;
                                        })}
                                    </select>
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="control-label col-md-2" htmlFor="cat-desc">Description</label>
                                <div className="col-md-10">
                                    <input className="form-control" id="cat-desc" name="Description" type="text" onChange={this.onDescChange} value={this.state.description}/>
                                </div>
                            </div>
                        </ReactModalBody>
                        <ReactModalFooter>
                            <ReactBtn bsStyle="primary" className="pull-right" type="submit">Submit</ReactBtn>
                        </ReactModalFooter>
                    </form>
                </ReactModal>
            </div>
        );
    }

}

class EditContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            parentId: this.props.cat.ParentId,
            name: this.props.cat.Name,
            description: this.props.cat.Description,
            errors: []
        };

        this.onChangeDesc = this.onChangeDesc.bind(this);
        this.onChangeName = this.onChangeName.bind(this);
        this.onParentChange = this.onParentChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.notify = this.notify.bind(this);
    }
    onChangeDesc(e) {
        this.setState({ description: e.target.value });
    }
    onChangeName(e) {
        this.setState({ name: e.target.value });
    }
    onParentChange(e) {
        this.setState({ parentId: e.target.value });
    }
    onSubmit(e) {
        e.preventDefault();

        $.ajax({
            context: this,
            url: document.getElementById("UpdateAction").value,
            method: "post",
            data: {
                __RequestVerificationToken: document.querySelector("[name='__RequestVerificationToken']").value,
                Id: this.props.cat.Id,
                ParentId: this.state.parentId,
                Name: this.state.name,
                Description: this.state.description
            },
            success: function(response) {
                if (!response.Errors) {
                    this.props.refreshList();
                    this.props.unsetEditMode();
                    this.notify("Updated!", "You have updated a category.", "fa-exclamation", "info");
                } else {
                    this.notify("Error!", response.ErrCol, "fa-times", "error");
                    this.setState({ errors: response.ErrCol });
                }
            }
        });
    }
    notify(title, message, icon, type = "info") {
        switch(type) {
            case "error":
                Cheers.error({
                    title: title,
                    message: message,
                    alert: "slideleft",
                    icon: icon,
                    duration: 3
                });
                break;
            case "info":
            default:
                Cheers.info({
                    title: title,
                    message: message,
                    alert: "slideleft",
                    icon: icon,
                    duration: 3
                });
                break;
        }
        
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            parentId: nextProps.cat.ParentId,
            name: nextProps.cat.Name,
            description: nextProps.cat.Description
        });
    }
    render() {
        return (
            <div className="col-md-4">
                <form className="form-horizontal" method="post" onSubmit={this.onSubmit}>
                    <div className="box box-primary">
                        <div className="box-header">
                            <div className="box-title">Edit Category</div>
                            <button type="button" className="close" aria-label="Close" onClick={this.props.unsetEditMode}>
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="box-body">
                            <input id="edit-id" type="hidden" value={this.props.cat.Id} />
                            <div className="form-group">
                                <label className="control-label col-md-2" htmlFor="edit-name">Name</label>
                                <div className="col-md-10">
                                    <input className="form-control" id="edit-name" type="text" onChange={this.onChangeName} value={this.state.name || ""} />
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="control-label col-md-2" htmlFor="cat-parent">Parent</label>
                                <div className="col-md-10">
                                    <select className="form-control" id="cat-parent" name="ParentId" onChange={this.onParentChange} value={this.state.parentId}>
                                        {this.props.modalOpts.map(function(cat) {
                                            return <option value={cat.Id} key={cat.Id}>{cat.Name}</option>;
                                        }, this)}
                                    </select>
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="control-label col-md-2" htmlFor="edit-name">Desccription</label>
                                <div className="col-md-10">
                                    <input className="form-control" id="edit-name" type="text" onChange={this.onChangeDesc} value={this.state.description || ""} />
                                </div>
                            </div>
                        </div>
                        <div className="box-footer">
                            <button className="btn btn-default" onClick={this.props.unsetEditMode}>Cancel</button>
                            <button className="btn btn-primary pull-right" type="submit">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

class Container extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            categories: [],
            edit: false,
            selectedCat: {},
            modalOpts: [],
            checkedCat: []
        };

        this.setEditMode = this.setEditMode.bind(this);
        this.unsetEditMode = this.unsetEditMode.bind(this);
        this.unsetEditModeKeyPress = this.unsetEditModeKeyPress.bind(this);
        this.refreshList = this.refreshList.bind(this);
        this.checkCategory = this.checkCategory.bind(this);
        this.uncheckCategory = this.uncheckCategory.bind(this);
        this.isCatChecked = this.isCatChecked.bind(this);
        this.allCatSelector = this.allCatSelector.bind(this);
        this.queueDelete = this.queueDelete.bind(this);
        this.resetQueueDelete = this.resetQueueDelete.bind(this);
    }
    resetQueueDelete() {
        document.getElementById("chk-delall-top").checked = false;
        document.getElementById("chk-delall-bot").checked = false;
        this.setState({ checkedCat: [] });
    }
    queueDelete() {
        return this.state.checkedCat;
    }
    allCatSelector(event) {
        event.persist();
        var checked = event.target.checked;
        if (checked) {
            confirmAlert({
                title: "Warning",
                message: "You are attempting to delete all categories. Are you sure you want to continue?",
                confirmLabel: "Confirm",
                cancelLabel: "Cancel",
                onConfirm: () => {
                    document.getElementById("chk-delall-top").checked = true;
                    document.getElementById("chk-delall-bot").checked = true;
                    var col = [];
                    $.each(this.state.categories, function(i, cat) {
                        col.push({
                            Id: cat.Id, 
                            Parent: cat.parentId,
                            IsParent: (cat.ParentId == null)
                        });
                    });
                    this.setState({ checkedCat: col });
                },
                onCancel: () => {
                    document.getElementById("chk-delall-top").checked = false;
                    document.getElementById("chk-delall-bot").checked = false;
                }
            });
        } else {
            this.resetQueueDelete();
        }
    }
    checkCategory(id) {
        var col = this.state.checkedCat;

        if (col.findIndex(x => x.Id === id) === -1) {
            var cat = this.state.categories.find(x => x.Id === id);
            col.push({
                Id: id, 
                Parent: cat.parentId,
                IsParent: (cat.ParentId == null)
            });
            this.setState({ checkedCat: col });
        }
    }
    uncheckCategory(id) {
        var index = this.state.checkedCat.findIndex(x => x.Id === id);

        if (index > -1) {
            var col = this.state.checkedCat;
            col.splice(index, 1);
            this.setState({ checkedCat: col });
        }
    }
    isCatChecked(id) {
        return this.state.checkedCat.findIndex(x => x.Id === id) > -1;
    }
    setEditMode(category) {
        this.setState({ edit: true, selectedCat: category });
    }
    unsetEditMode() {
        this.setState({ edit: false });
    }
    unsetEditModeKeyPress(e) {
        if (e.keyCode === 27) {
            this.setState({ edit: false });
        }
    }
    refreshList() {
        $.ajax({
            cache: false,
            context: this,
            url: document.getElementById("TableUrl").value,
            method: "get",
            success: function(response) {
                var load = JSON.parse(response);
                this.setState({ categories: load.Categories });
            }
        });

        $.ajax({
            cache: false,
            context: this,
            url: document.getElementById("ModalDataUrl").value,
            method: "get",
            success: function(response) {
                this.setState({ modalOpts: response.Categories });
            }
        });
    }
    componentWillMount() {
        this.refreshList();
        document.addEventListener("keydown", this.unsetEditModeKeyPress);
    }
    componentWillUnmount() {
        document.removeEventListener("keydown", this.unsetEditModeKeyPress);
    }
    render() {
        var containerCls = (this.state.edit) ? "col-md-8 table-responsive" : "col-md-12 table-responsive";
        return (
            <div>
                <div className={containerCls}>
                    <CategoryContainer
                        categories={this.state.categories}
                        modalOpts={this.state.modalOpts}
                        setEditMode={this.setEditMode}
                        unsetEditMode={this.unsetEditMode}
                        refreshList={this.refreshList}
                        checkCategory={this.checkCategory}
                        uncheckCategory={this.uncheckCategory}
                        isCatChecked={this.isCatChecked}
                        allCatSelector={this.allCatSelector}
                        queueDelete={this.queueDelete}
                        resetQueueDelete={this.resetQueueDelete} />
                </div>
                    {this.state.edit &&
                    <EditContainer
                    unsetEditMode={this.unsetEditMode}
                    cat={this.state.selectedCat}
                    modalOpts={this.state.modalOpts}
                    refreshList={this.refreshList} />}
            </div>
        );
    }
}

ReactDOM.render(<Container />, document.getElementById("category-view-container"))