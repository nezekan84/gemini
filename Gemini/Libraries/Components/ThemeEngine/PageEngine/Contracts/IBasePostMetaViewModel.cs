﻿using System;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.Factory;
using Gemini.Libraries.Models;

namespace Gemini.Libraries.Components.ThemeEngine.PageEngine.Contracts
{
    /// <summary>
    /// Since: Rev 12a
    /// Current: Rev 4
    /// </summary>
    public interface IBasePostMetaViewModel : IViewModelFactoryItem
    {
        int Id { get; set; }

        int PostId { get; set; }

        Posts Post { get; set; }

        string Meta { get; set; }

        string Value { get; set; }

        DateTime DateUpdated { get; set; }

        DateTime DateCreated { get; set; }
    }
}
