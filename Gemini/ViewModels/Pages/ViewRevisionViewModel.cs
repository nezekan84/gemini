﻿using System;
using Gemini.Libraries.Models;

namespace Gemini.ViewModels.Pages
{
    public class ViewRevisionViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public int RevisionId { get; set; }

        public string RevisionTitle { get; set; }

        public string RevisionContent { get; set; }
    }
}