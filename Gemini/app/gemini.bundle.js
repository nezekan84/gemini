/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 438);
/******/ })
/************************************************************************/
/******/ ({

/***/ 438:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

$(document).ready(function () {
    var Sidebar = function () {
        function Sidebar() {
            _classCallCheck(this, Sidebar);

            // location obj
            this.location = window.location;
            // window.location.pathname via split
            this.path = [];
            this.treeProvider = {};
        }

        _createClass(Sidebar, [{
            key: "TreeSelector",
            value: function TreeSelector() {
                this.path = this.location.pathname.split(/\//);

                if (this.path[1] === "") {
                    this.treeProvider.Index(this.path);
                } else {
                    // to ensure that convention is followed regardless of route case
                    var tree = this.path[1].charAt(0).toUpperCase() + this.path[1].slice(1).toLowerCase();

                    this.treeProvider[tree](this.path);
                }
            }
        }]);

        return Sidebar;
    }();

    var TreeProvider = function () {
        function TreeProvider() {
            _classCallCheck(this, TreeProvider);
        }

        _createClass(TreeProvider, [{
            key: "Index",
            value: function Index() {
                this.MarkRoot("index");
            }
        }, {
            key: "Pages",
            value: function Pages(path) {
                this.MarkRoot("pages");
                var tree = new PagesTreeMarker(path);
                tree.Exec();
            }
        }, {
            key: "Categories",
            value: function Categories(path) {
                this.MarkRoot("posts");
                var tree = new PostsTreeMarker(path);
                tree.Exec();
            }
        }, {
            key: "Posts",
            value: function Posts(path) {
                this.MarkRoot("posts");
                var tree = new PostsTreeMarker(path);
                tree.Exec();
            }
        }, {
            key: "Tags",
            value: function Tags(path) {
                this.MarkRoot("posts");
                var tree = new PostsTreeMarker(path);
                tree.Exec();
            }
        }, {
            key: "Appearance",
            value: function Appearance(path) {
                this.MarkRoot("appearance");
                var tree = new AppearanceTreeMarker(path);
                tree.Exec();
            }
        }, {
            key: "Settings",
            value: function Settings(path) {
                this.MarkRoot("settings");
                var tree = new SettingsTreeMarker(path);
                tree.Exec();
            }
        }, {
            key: "MarkRoot",
            value: function MarkRoot(root) {
                // have to mark root as active.
                document.querySelector("li#" + root).classList.add("active");
            }
        }]);

        return TreeProvider;
    }();

    var BaseTreeMarker = function BaseTreeMarker() {
        _classCallCheck(this, BaseTreeMarker);

        this.root = "";
        this.path = null;
        this.treeSelector = "";
        this.bindings = new Map();
    };

    var PagesTreeMarker = function (_BaseTreeMarker) {
        _inherits(PagesTreeMarker, _BaseTreeMarker);

        function PagesTreeMarker(path) {
            _classCallCheck(this, PagesTreeMarker);

            var _this = _possibleConstructorReturn(this, (PagesTreeMarker.__proto__ || Object.getPrototypeOf(PagesTreeMarker)).call(this));

            _this.root = "/Pages";
            _this.path = path;
            _this.treeSelector = "li#pages";
            // bindings (path -> nav item)
            _this.bindings = new Map([["View", _this.root], ["revision", _this.root]]);
            return _this;
        }

        _createClass(PagesTreeMarker, [{
            key: "Exec",
            value: function Exec() {
                if (this.path[2] === undefined) {
                    document.querySelector(this.treeSelector + " ul>li").classList.add("active");
                } else {
                    var isFound = false;
                    var pathName = "/" + this.path[1] + "/" + this.path[2];
                    var treeSelector = this.treeSelector;

                    $(treeSelector + " li>a").each(function (i, dom) {
                        if (dom.pathname === pathName) {
                            // set as found to prevent further processing.
                            isFound = true;

                            document.querySelector(treeSelector + " a[href=\"" + dom.pathname + "\"]").closest("li").classList.add("active");
                        }
                    });

                    if (!isFound) {
                        // get bound route.
                        var bound = this.bindings.get(this.path[2]);
                        // mark nav item as active
                        document.querySelector(this.treeSelector + " a[href=\"" + bound + "\"]").closest("li").classList.add("active");
                    }
                }
            }
        }]);

        return PagesTreeMarker;
    }(BaseTreeMarker);

    var PostsTreeMarker = function (_BaseTreeMarker2) {
        _inherits(PostsTreeMarker, _BaseTreeMarker2);

        function PostsTreeMarker(path) {
            _classCallCheck(this, PostsTreeMarker);

            var _this2 = _possibleConstructorReturn(this, (PostsTreeMarker.__proto__ || Object.getPrototypeOf(PostsTreeMarker)).call(this));

            _this2.path = path;
            _this2.treeSelector = "li#posts";

            return _this2;
        }

        _createClass(PostsTreeMarker, [{
            key: "Exec",
            value: function Exec() {
                if (this.path[2] === undefined) {
                    document.querySelector(this.treeSelector + " a[href=\"" + window.location.pathname + "\"]").closest("li").classList.add("active");
                }
            }
        }]);

        return PostsTreeMarker;
    }(BaseTreeMarker);

    var AppearanceTreeMarker = function (_BaseTreeMarker3) {
        _inherits(AppearanceTreeMarker, _BaseTreeMarker3);

        function AppearanceTreeMarker(path) {
            _classCallCheck(this, AppearanceTreeMarker);

            var _this3 = _possibleConstructorReturn(this, (AppearanceTreeMarker.__proto__ || Object.getPrototypeOf(AppearanceTreeMarker)).call(this));

            _this3.path = path;
            _this3.treeSelector = "li#appearance";
            return _this3;
        }

        _createClass(AppearanceTreeMarker, [{
            key: "Exec",
            value: function Exec() {
                if (this.path[2] === undefined) {
                    document.querySelector(this.treeSelector + " ul>li").classList.add("active");
                }
            }
        }]);

        return AppearanceTreeMarker;
    }(BaseTreeMarker);

    var SettingsTreeMarker = function (_BaseTreeMarker4) {
        _inherits(SettingsTreeMarker, _BaseTreeMarker4);

        function SettingsTreeMarker(path) {
            _classCallCheck(this, SettingsTreeMarker);

            var _this4 = _possibleConstructorReturn(this, (SettingsTreeMarker.__proto__ || Object.getPrototypeOf(SettingsTreeMarker)).call(this));

            _this4.path = path;
            _this4.treeSelector = "li#settings";
            return _this4;
        }

        _createClass(SettingsTreeMarker, [{
            key: "Exec",
            value: function Exec() {
                document.querySelector(this.treeSelector + " a[href=\"" + window.location.pathname + "\"]").closest("li").classList.add("active");
            }
        }]);

        return SettingsTreeMarker;
    }(BaseTreeMarker);

    var sidebar = new Sidebar();
    sidebar.treeProvider = new TreeProvider();
    sidebar.TreeSelector();
});

/***/ })

/******/ });