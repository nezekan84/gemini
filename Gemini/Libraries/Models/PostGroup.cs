﻿using System;
using System.Collections.Generic;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.Enums;

namespace Gemini.Libraries.Models
{
    /// <summary>
    /// Since: Rev ?a
    /// Current: Rev 4
    /// </summary>
    public class PostGroup
    {
        public int Id { get; set; }

        public PostGroupType Type { get; set; }

        public string Name { get; set; }

        public string Slug { get; set; }

        public string Description { get; set; }

        public int? ParentId { get; set; }

        public PostGroup Parent { get; set; }

        public ICollection<PostGroup> Children { get; set; }

        public DateTime DateCreated { get; set; }

        public ICollection<Posts> Posts { get; set; }
    }
}