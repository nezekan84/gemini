﻿namespace Gemini.Libraries.Components.Settings
{
    /// <summary>
    /// Since: Rev 19a
    /// Current: Rev 19a
    /// </summary>
    public class Entities
    {
        // entity for global or site-wide settings
        public static string Global => "global";

        // entity for page settings
        public static string Page => "page";

        // entity for user settings
        public static string User => "user";
    }
}