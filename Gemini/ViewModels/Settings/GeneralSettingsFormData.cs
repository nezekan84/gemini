﻿namespace Gemini.ViewModels.Settings
{
    public class GeneralSettingsFormData
    {
        public string FormAction { get; set; }

        public string SiteTitle { get; set; }

        public string SiteTagline { get; set; }
    }
}