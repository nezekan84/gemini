﻿using System;
using System.Reflection;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.Factory;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.PostViewModels;

namespace Gemini.Libraries.Components.ThemeEngine
{
    /// <summary>
    /// ViewModels set here will only be used as placeholders.
    /// Ones used will come from ViewModelFactory via EngineState.
    /// 
    /// Since: Rev 17a
    /// Current: Rev 4
    /// </summary>
    public class ViewModels
    {
        private readonly ViewModelFactory _factory = EngineState.Instance.ViewModelFactory;

        /// <summary>
        /// 
        /// </summary>
        public BasePageViewModel Page { get; set; } = new BasePageViewModel();

        /// <summary>
        /// Reset values.
        /// </summary>
        public void Reset()
        {
            PropertyInfo[] props = GetType().GetProperties();

            foreach (PropertyInfo prop in props)
            {
                Type type = prop.GetType();
                prop.SetValue(this, type.FullName == typeof(IViewModelFactoryItem).FullName ? _factory.Get(type) : null);
            }
        }
    }
}