namespace Gemini.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatePostMetaTbl : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PostMeta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PostId = c.Int(nullable: false),
                        Meta = c.String(),
                        Value = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Posts", t => t.PostId, cascadeDelete: true)
                .Index(t => t.PostId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PostMeta", "PostId", "dbo.Posts");
            DropIndex("dbo.PostMeta", new[] { "PostId" });
            DropTable("dbo.PostMeta");
        }
    }
}
