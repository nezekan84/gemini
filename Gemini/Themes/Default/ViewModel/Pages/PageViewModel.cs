﻿using Gemini.Libraries.Components.ThemeEngine.PageEngine.PostViewModels;
using Gemini.Themes.Default.ViewModel.PageMeta;

namespace Gemini.Themes.Default.ViewModel.Pages
{
    public class PageViewModel : BasePageViewModel
    {
        public PageViewModel()
        {
            Meta.Add(new TestAttr());
            Meta.Add(new TestAttr2());
        }
    }
}