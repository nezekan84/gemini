namespace Gemini.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExtendPostGroupTbl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PostGroup", "Slug", c => c.String());
            AddColumn("dbo.PostGroup", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PostGroup", "Description");
            DropColumn("dbo.PostGroup", "Slug");
        }
    }
}
