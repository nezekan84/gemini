﻿using Gemini.Libraries.Components.ThemeEngine;
using Gemini.Themes.Classic;
using Gemini.Themes.Default;

namespace Gemini.Themes
{
    /// <summary>
    /// Since: Rev 7
    /// Current: Rev 17
    /// </summary>
    public class ThemeRegistrar : BaseThemeServiceEngine
    {
        /// <summary>
        /// Register your themes here.
        /// All theme service providers MUST implement BaseThemeServiceProvider class.
        /// All theme service providers MUST follow this convention: {Theme name}ThemeServiceProvider.
        /// </summary>
        public override void Boot()
        {
            Add(new DefaultThemeServiceProvider());
            Add(new ClassicThemeServiceProvider());

            // DO NOT DELETE THIS LINE!
            Initialize();
        }
    }
}