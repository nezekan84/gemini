﻿import Cheers from "cheers-alert";
import React from "react";
import Renderer from "react-dom";

// this should be used where pages do not have react and reactdom declared on a separate file.
// this should be used with Gemini.Libraries.Components.Toast.Notify class.
// TO DO: add support for multiple notifs.
class Notif extends React.Component {
    trigger() {
        if (document.getElementById("Notify_Show").value === "True") {
            var props = {
                alert: document.getElementById("Notify_Alert").value,
                duration: document.getElementById("Notify_Duration").value,
                icon: document.getElementById("Notify_Icon").value,
                message: document.getElementById("Notify_Message").value,
                //stacking: document.getElementById("Notify_Stacking").value,
                title: document.getElementById("Notify_Title").value,
                toggle: document.getElementById("Notify_Toggle").value
            };

            switch(document.getElementById("Notify_Type").value) {
                case "success":
                    Cheers.success(props);
                    break;
                case "warning":
                    Cheers.warning(props);
                    break;
                case "error":
                    Cheers.error(props);
                    break;
                case "info":
                default:
                    Cheers.info(props);
                    break;
            }
        }
    }
    render() {
        return (
                <div>{this.trigger()}</div>
            )
    }
}

Renderer.render(<Notif />, document.getElementById("notif-area"))