using Gemini.Libraries.Components.Settings;

namespace Gemini.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedThemeSetting : DbMigration
    {
        private string _entity = Entities.Global;
        private string _attrib = Attributes.Theme;

        public override void Up()
        {
            var query =
                $"INSERT INTO [dbo].[Settings] ([UserId], [Entity], [Attribute], [DateCreated], [Value]) VALUES (N'00ef4ed7-d7c8-4b85-b1a9-038d467124c8', N'{_entity}', N'{_attrib}', N'2017-03-07 23:48:51', N'Default')";
                
            Sql(query);
        }
        
        public override void Down()
        {
            var query = $"DELETE FROM [dbo].[Settings] WHERE [Entity] = N'{_entity}' AND [Attribute] = N'{_attrib}'";

            Sql(query);
        }
    }
}
