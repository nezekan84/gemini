namespace Gemini.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Datetime2Fix : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Posts", "DateUpdated");
            DropColumn("dbo.PostMeta", "DateUpdated");

            AddColumn("dbo.PostMeta", "DateUpdated", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.PostMeta", "DateCreated", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Posts", "DateUpdated", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AlterColumn("dbo.Posts", "DateCreated", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Posts", "DateUpdated");
            DropColumn("dbo.PostMeta", "DateUpdated");

            AlterColumn("dbo.Posts", "DateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.Posts", "DateUpdated", c => c.DateTime(nullable: false));
            AlterColumn("dbo.PostMeta", "DateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.PostMeta", "DateUpdated", c => c.DateTime(nullable: false));
        }
    }
}
