﻿using System.Data.Entity.ModelConfiguration;

namespace Gemini.Libraries.Models.Configurations
{
    /// <summary>
    /// Since: Rev ?a
    /// Current: Rev ?a
    /// </summary>
    public class PostGroupConfig : EntityTypeConfiguration<PostGroup>
    {
        public PostGroupConfig()
        {
            ToTable("PostGroup");

            // columns
            Property(x => x.Type)
                .HasColumnType("tinyint");

            // relationships
            HasOptional(child => child.Parent)
                .WithMany(parent => parent.Children)
                .HasForeignKey(child => child.ParentId)
                .WillCascadeOnDelete(false);

            HasMany(group => group.Posts)
                .WithMany(posts => posts.Group)
                .Map(m =>
                {
                    m.ToTable("PostGroupings");
                    // PostGroup.Id <-> Posts.PostId
                    m.MapLeftKey("PostGroupId").MapRightKey("PostId");
                });
        }
    }
}