﻿using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Gemini.Libraries.Components
{
    /// <summary>
    /// Special thanks to dana of Stack Overflow (http://stackoverflow.com/questions/2920744/url-slugify-algorithm-in-c)
    /// 
    /// Since: Rev 13a
    /// Current: Rev 26a
    /// </summary>
    public class UrlSlugger : DbContext
    {
        // white space, em-dash, en-dash, underscore
        static readonly Regex WordDelimiters = new Regex(@"[\s—–_]", RegexOptions.Compiled);

        // characters that are not valid
        static readonly Regex InvalidChars = new Regex(@"[^a-z0-9\-]", RegexOptions.Compiled);

        // multiple hyphens
        static readonly Regex MultipleHyphens = new Regex(@"-{2,}", RegexOptions.Compiled);

        public static string ToUrlSlug(string value)
        {
            // convert to lower case
            value = value.ToLowerInvariant();

            // remove diacritics (accents)
            value = RemoveDiacritics(value);

            // ensure all word delimiters are hyphens
            value = WordDelimiters.Replace(value, "-");

            // strip out invalid characters
            value = InvalidChars.Replace(value, "");

            // replace multiple hyphens (-) with a single hyphen
            value = MultipleHyphens.Replace(value, "-");

            // trim hyphens (-) from ends
            return value.Trim('-');
        }

        /// See: http://www.siao2.com/2007/05/14/2629747.aspx
        private static string RemoveDiacritics(string stIn)
        {
            string stFormD = stIn.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();

            for (int ich = 0; ich < stFormD.Length; ich++)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(stFormD[ich]);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(stFormD[ich]);
                }
            }

            return (sb.ToString().Normalize(NormalizationForm.FormC));
        }

        /// <summary>
        /// Create URL slug with id as prefix
        /// </summary>
        /// <param name="title"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string Slugify(string title, int id)
        {
            return ToUrlSlug(id + " " + title);
        }

        /// <summary>
        /// Create URL slug with highest auto_increment val as prefix.
        /// </summary>
        /// <param name="title"></param>
        /// <returns></returns>
        public static string Slugify(string title)
        {
            UrlSlugger instance = new UrlSlugger();
            int? max = instance.Context.Posts.Max(p => (int?)p.PostId);

            return max == null ? ToUrlSlug(title) : ToUrlSlug((max + 1) + " " + title);
        }
    }
}