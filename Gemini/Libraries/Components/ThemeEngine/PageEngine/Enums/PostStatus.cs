﻿using System.ComponentModel.DataAnnotations;

namespace Gemini.Libraries.Components.ThemeEngine.PageEngine.Enums
{
    /// <summary>
    /// Since: Rev 13a
    /// Current: Revv 4
    /// </summary>
    public enum PostStatus
    {
        [Display(Name = "Draft")]
        Draft = 0,

        [Display(Name = "Final")]
        Final = 1
    }
}