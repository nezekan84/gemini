namespace Gemini.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatePostGroupTbls : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PostGroup",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Byte(nullable: false),
                        Name = c.String(),
                        ParentId = c.Int(),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.PostGroup", t => t.ParentId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.PostGroupings",
                c => new
                    {
                        PostGroupId = c.Int(nullable: false),
                        PostId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostGroupId, t.PostId })
                .ForeignKey("dbo.PostGroup", t => t.PostGroupId, cascadeDelete: true)
                .ForeignKey("dbo.Posts", t => t.PostId, cascadeDelete: true)
                .Index(t => t.PostGroupId)
                .Index(t => t.PostId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PostGroupings", "PostId", "dbo.Posts");
            DropForeignKey("dbo.PostGroupings", "PostGroupId", "dbo.PostGroup");
            DropForeignKey("dbo.PostGroup", "ParentId", "dbo.PostGroup");
            DropIndex("dbo.PostGroupings", new[] { "PostId" });
            DropIndex("dbo.PostGroupings", new[] { "PostGroupId" });
            DropIndex("dbo.PostGroup", new[] { "ParentId" });
            DropTable("dbo.PostGroupings");
            DropTable("dbo.PostGroup");
        }
    }
}
