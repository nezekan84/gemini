namespace Gemini.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatePostsTable : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Settings", new[] { "UserId" });
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        Title = c.String(),
                        Slug = c.String(),
                        Content = c.String(maxLength: 2000),
                        PostType = c.Int(nullable: false),
                        ParentId = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Posts", t => t.ParentId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.ParentId);
            
            AlterColumn("dbo.Settings", "UserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Settings", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Posts", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Posts", "ParentId", "dbo.Posts");
            DropIndex("dbo.Settings", new[] { "UserId" });
            DropIndex("dbo.Posts", new[] { "ParentId" });
            DropIndex("dbo.Posts", new[] { "UserId" });
            AlterColumn("dbo.Settings", "UserId", c => c.String(maxLength: 128));
            DropTable("dbo.Posts");
            CreateIndex("dbo.Settings", "UserId");
        }
    }
}
