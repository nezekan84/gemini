﻿namespace Gemini.Libraries.Components.ThemeEngine.PageEngine.Factory
{
    /// <summary>
    /// Interface to enable usage in ViewModelFactory
    /// 
    /// Since: Rev 17a
    /// Current: Rev 4
    /// </summary>
    public interface IViewModelFactoryItem
    {
    }
}