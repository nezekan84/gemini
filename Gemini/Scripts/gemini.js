﻿$(document).ready(function () {
    class Sidebar
    {
        constructor() {
            // location obj
            this.location = window.location;
            // window.location.pathname via split
            this.path = [];
            this.treeProvider = {};
        }

        TreeSelector() {
            this.path = this.location.pathname.split(/\//);

            if (this.path[1] === "") {
                this.treeProvider.Index(this.path);
            } else {
                // to ensure that convention is followed regardless of route case
                const tree = this.path[1].charAt(0).toUpperCase() + this.path[1].slice(1).toLowerCase();
                
                this.treeProvider[tree](this.path);
            }
        }
    }

    class TreeProvider
    {
        Index() {
            this.MarkRoot("index");
        }

        Pages(path) {
            this.MarkRoot("pages");
            const tree = new PagesTreeMarker(path);
            tree.Exec();
        }

        Categories(path) {
            this.MarkRoot("posts");
            const tree = new PostsTreeMarker(path);
            tree.Exec();
        }

        Posts(path) {
            this.MarkRoot("posts");
            const tree = new PostsTreeMarker(path);
            tree.Exec();
        }

        Tags(path) {
            this.MarkRoot("posts");
            const tree = new PostsTreeMarker(path);
            tree.Exec();
        }

        Appearance(path) {
            this.MarkRoot("appearance");
            const tree = new AppearanceTreeMarker(path);
            tree.Exec();
        }

        Settings(path) {
            this.MarkRoot("settings");
            const tree = new SettingsTreeMarker(path);
            tree.Exec();
        }

        MarkRoot(root) {
            // have to mark root as active.
            document.querySelector(`li#${root}`).classList.add("active");
        }
    }

    class BaseTreeMarker {
        constructor() {
            this.root = "";
            this.path = null;
            this.treeSelector = "";
            this.bindings = new Map();
        }
    }

    class PagesTreeMarker extends BaseTreeMarker
    {
        constructor(path) {
            super();

            this.root = "/Pages";
            this.path = path;
            this.treeSelector = "li#pages";
            // bindings (path -> nav item)
            this.bindings = new Map([
                ["View", this.root],
                ["revision", this.root]
            ]);
        }

        Exec() {
            if (this.path[2] === undefined) {
                document.querySelector(`${this.treeSelector} ul>li`)
                    .classList
                    .add("active");
            } else {
                var isFound = false;
                var pathName = `/${this.path[1]}/${this.path[2]}`;
                var treeSelector = this.treeSelector;

                $(`${treeSelector} li>a`).each(function (i, dom) {
                    if (dom.pathname === pathName) {
                        // set as found to prevent further processing.
                        isFound = true;

                        document.querySelector(`${treeSelector} a[href="${dom.pathname}"]`)
                            .closest("li")
                            .classList
                            .add("active");
                    }
                });

                if (!isFound) {
                    // get bound route.
                    var bound = this.bindings.get(this.path[2]);
                    // mark nav item as active
                    document.querySelector(`${this.treeSelector} a[href="${bound}"]`)
                        .closest("li")
                        .classList
                        .add("active");
                }
            }
        }
    }

    class PostsTreeMarker extends BaseTreeMarker
    {
        constructor(path) {
            super();

            this.path = path;
            this.treeSelector = "li#posts";

        }

        Exec() {
            if (this.path[2] === undefined) {
                document.querySelector(`${this.treeSelector} a[href="${window.location.pathname}"]`).closest("li")
                    .classList.add("active");
            }
        }
    }

    class AppearanceTreeMarker extends BaseTreeMarker
    {
        constructor(path) {
            super();

            this.path = path;
            this.treeSelector = "li#appearance";
        }

        Exec() {
            if (this.path[2] === undefined) {
                document.querySelector(`${this.treeSelector} ul>li`).classList.add("active");
            }
        }
    }

    class SettingsTreeMarker extends BaseTreeMarker
    {
        constructor(path) {
            super();

            this.path = path;
            this.treeSelector = "li#settings";
        }

        Exec() {
            document.querySelector(`${this.treeSelector} a[href="${window.location.pathname}"]`).closest("li").classList.add("active");
        }
    }


    var sidebar = new Sidebar();
    sidebar.treeProvider = new TreeProvider();
    sidebar.TreeSelector();
});