﻿using System;

namespace Gemini.Libraries.Models
{
    /// <summary>
    /// Since: Rev ?a
    /// Current: Rev ?a
    /// </summary>
    public class PostMeta
    {
        public int Id { get; set; }

        public int PostId { get; set; }

        public Posts Post { get; set; }

        public string Theme { get; set; }

        public string Meta { get; set; }

        public string Value { get; set; }

        public DateTime DateUpdated { get; set; }

        public DateTime DateCreated { get; set; }
    }
}