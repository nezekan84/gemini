﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Gemini.Libraries.Components;
using Gemini.Libraries.Components.ThemeEngine;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.Enums;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.PostViewModels;
using Gemini.Libraries.Components.Toast;
using Gemini.Libraries.Models;
using Gemini.Libraries.ViewModels.Pages;
using Gemini.Models;
using Gemini.Themes;
using Gemini.Themes.Default.ViewModel.Pages;
using Gemini.ViewModels.Pages;
using Microsoft.AspNet.Identity;
using PagedList;

namespace Gemini.Controllers
{
    /// <summary>
    /// Since: Rev ?a
    /// Current: Rev 4
    /// </summary>
    [Authorize]
    public class PagesController : Controller
    {
        private readonly ApplicationDbContext _context = new ApplicationDbContext();
        private readonly ThemeRegistrar _themeRegistrar = new ThemeRegistrar();

        // GET: Pages
        public ViewResult Index(int? page)
        {
            int pageIndex = (page ?? 1) - 1;
            int pageSize = 10;
            int skip = (pageIndex == 0) ? 0 : pageIndex * pageSize;

            List<Posts> pages = _context.Posts
                .Where(x => x.PostType == PostTypes.Page)
                .OrderBy(x => x.PostId)
                .Skip(skip)
                .Take(pageSize)
                .ToList();

            List<PageViewModel> pageVMs = pages.Select(pageVm => new PageViewModel()
            {
                Id = pageVm.PostId,
                Title = pageVm.Title,
                Slug = pageVm.Slug,
                Status = pageVm.PostStatus,
                DateCreated = pageVm.DateCreated,
                DateUpdated = pageVm.DateUpdated
            }).ToList();

            PageListViewModel vm = new PageListViewModel
            {
                Pages =
                    new StaticPagedList<PageViewModel>(pageVMs, pageIndex + 1, pageSize,
                        _context.Posts.Count(x => x.PostType == PostTypes.Page))
            };
            return View(vm);
        }

        public ViewResult Create()
        {
            // retrieve view model with all of its meta from theme registrar
            return View(_themeRegistrar.GetPageViewModel());
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult ProcessNew(BasePageViewModel form)
        {
            if (!ModelState.IsValid)
                return View("Create", form);

            Posts post = new Posts()
            {
                Title = form.Title,
                Slug = UrlSlugger.Slugify(form.Title),
                Content = form.Content,
                UserId = HttpContext.User.Identity.GetUserId(),
                PostType = form.Type,
                PostStatus = form.Status,
                DateCreated = DateTime.Now,
                DateUpdated = DateTime.Now,
                Visibility = form.Visibility
            };
            _context.Posts.Add(post);

            foreach (BasePostMetaViewModel formMeta in form.Meta)
            {
                var themeName = _themeRegistrar.CurrentServiceProvider().ThemeName;
                _context.PostMeta.Add(new PostMeta()
                {
                    Post = post,
                    DateCreated = DateTime.Now,
                    Theme = themeName,
                    Meta = formMeta.Meta,
                    Value = formMeta.Value
                });
            }
            _context.SaveChanges();

            return RedirectToAction("View", new { id = post.PostId });
        }

        public ActionResult View(int id, bool? updated)
        {
            Posts model = _context.Posts.SingleOrDefault(post => post.PostId == id);

            if (model == null)
                return RedirectToAction("Index");

            string themeName = _themeRegistrar.CurrentServiceProvider().ThemeName;
            List<PostMeta> metaCol =
                _context.PostMeta
                .Where(x => x.PostId == id && x.Theme == themeName)
                    .ToList();

            BasePageViewModel vm = _themeRegistrar.GetEditPageViewModel();
            vm.Id = model.PostId;
            vm.UserId = model.UserId;
            vm.Title = model.Title;
            vm.Slug = model.Slug;
            vm.Content = model.Content;
            vm.Type = model.PostType;
            vm.Status = model.PostStatus;
            vm.Visibility = model.Visibility;
            vm.DateUpdated = model.DateUpdated;
            vm.Revisions = _context.Posts
                .Include(x => x.User)
                .Where(x => x.ParentId == model.PostId && x.PostType == PostTypes.Revision)
                .OrderByDescending(x => x.DateCreated)
                .Select(rev => new RevisionViewModel()
                {
                    Id = rev.PostId,
                    ParentId = (int)rev.ParentId,
                    UserId = rev.UserId,
                    User = rev.User,
                    DateCreated = rev.DateCreated
                })
                .ToList();

            if (metaCol.Any())
            {
                foreach (PostMeta modelMeta in metaCol)
                {
                    var overrideMeta = vm.Meta.SingleOrDefault(x => x.Meta == modelMeta.Meta);
                    if (overrideMeta == null)
                        continue;

                    overrideMeta.Value = modelMeta.Value;
                }
            }

            vm.DateCreated = model.DateCreated;

            if (updated != null && (bool) updated)
            {
                vm.Notify.Show = true;
                vm.Notify.Type = Notify.TypeInfo;
                vm.Notify.Title = "Updated!";
                vm.Notify.Message = "Page updated.";
            }

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Update(BasePageViewModel form)
        {
            Posts model = _context.Posts.Include(p => p.Meta).SingleOrDefault(p => p.PostId == form.Id);

            if (model == null)
                return RedirectToActionPermanent("Index");

            // only create revision if content does not match.
            if (model.Content != form.Content)
                CreateRevision(model.PostId, model.Title, model.Content);

            model.Slug = model.Title == form.Title ? model.Slug : UrlSlugger.Slugify(form.Title, model.PostId);
            model.Title = form.Title;
            model.Content = form.Content;
            model.PostStatus = form.Status;
            model.Visibility = form.Visibility;
            model.DateUpdated = DateTime.Now;

            string activeTheme = EngineState.Instance.ActiveTheme;
            foreach (BasePostMetaViewModel formMeta in form.Meta.ToList())
            {
                PostMeta dbMeta = model.Meta.SingleOrDefault(x => x.Meta == formMeta.Meta);
                if (dbMeta == null)
                {
                    PostMeta newMeta = new PostMeta()
                    {
                        Post = model,
                        Meta = formMeta.Meta,
                        Value = formMeta.Value,
                        Theme = activeTheme,
                        DateCreated = DateTime.Now,
                        DateUpdated = DateTime.Now
                    };
                    _context.PostMeta.Add(newMeta);
                }
                else
                {
                    dbMeta.Value = formMeta.Value;
                    dbMeta.DateUpdated = DateTime.Now;
                }
            }

            _context.SaveChanges();
            return RedirectToActionPermanent("View", new { id = form.Id, updated = true });
        }

        [Route("pages/revision/{postId:int}/{revisionId:int}")]
        public ActionResult Revision(int postId, int revisionId)
        {
            ViewRevisionViewModel post = _context.Posts
                .Join(
                    _context.Posts,
                    parent => parent.PostId,
                    child => child.ParentId,
                    (posts, child) => new ViewRevisionViewModel()
                    {
                        Id = posts.PostId,
                        RevisionId = child.PostId,
                        Content = posts.Content,
                        RevisionContent = child.Content,
                        Title = posts.Title,
                        RevisionTitle = child.Title
                    })
                    .SingleOrDefault(x => x.Id == postId && x.RevisionId == revisionId);

            if (post == null)
                return RedirectToAction("Index");

            return View(post);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult RestoreRevision(ViewRevisionViewModel form)
        {
            Posts post = _context.Posts
                .SingleOrDefault(p => p.PostId == form.Id);
            Posts revision = _context.Posts
            .SingleOrDefault(rev => rev.PostId == form.RevisionId && rev.PostType == PostTypes.Revision);

            if (post == null || revision == null)
                return RedirectToAction("Index");

            CreateRevision(post.PostId, post.Title, post.Content);
            post.Title = revision.Title;
            post.Slug = UrlSlugger.Slugify(post.Title, post.PostId);
            post.Content = revision.Content;
            post.DateUpdated = DateTime.Now;
            _context.SaveChanges();

            return RedirectToAction("View", new { id = post.PostId });
        }

        private void CreateRevision(int id, string title, string content)
        {
            Posts rev = new Posts()
            {
                UserId = HttpContext.User.Identity.GetUserId(),
                Title = title,
                Content = content,
                PostType = PostTypes.Revision,
                ParentId = id,
                DateUpdated = DateTime.Now,
                DateCreated = DateTime.Now
            };
            _context.Posts.Add(rev);
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }
    }
}