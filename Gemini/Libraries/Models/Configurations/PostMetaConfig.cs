﻿using System.Data.Entity.ModelConfiguration;

namespace Gemini.Libraries.Models.Configurations
{
    /// <summary>
    /// Since: Rev ?a
    /// Current: Rev ?a
    /// </summary>
    public class PostMetaConfig : EntityTypeConfiguration<PostMeta>
    {
        public PostMetaConfig()
        {
            ToTable("PostMeta");

            // columns
            Property(m => m.DateCreated)
                .HasColumnType("datetime2");
            Property(m => m.DateUpdated)
                .HasColumnType("datetime2");
        }
    }
}