﻿using System.Web.Mvc;

namespace Gemini.Controllers
{
    /// <summary>
    /// Since: Rev ?a
    /// Current: Rev 4
    /// </summary>
    public class PostsController : Controller
    {
        // GET: Posts
        public ActionResult Index()
        {
            return View();
        }

        public ViewResult Create()
        {
            return View();
        }
    }
}