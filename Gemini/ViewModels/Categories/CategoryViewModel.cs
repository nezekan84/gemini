﻿using System.ComponentModel.DataAnnotations;

namespace Gemini.ViewModels.Categories
{
    public class CategoryViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Category is required.")]
        [StringLength(55, ErrorMessage = "Category should be 4 - 55 characters long.", MinimumLength = 4)]
        [Display(Name = "Category")]
        public string Name { get; set; }

        public int? ParentId { get; set; }

        [StringLength(255, ErrorMessage = "Description should be not exceed 255 characters long.")]
        public string Description { get; set; }

        public string ParentName { get; set; }

        [Display(Name = "Post Count")]
        public int PostCount { get; set; }
    }
}