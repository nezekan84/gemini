﻿using System;
using System.ComponentModel.DataAnnotations;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.Contracts;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.Enums;
using Gemini.Models;

namespace Gemini.Libraries.Components.ThemeEngine.PageEngine.PostViewModels
{
    /// <summary>
    /// Since: Rev 8a
    /// Current: Rev 4
    /// </summary>
    public class PostViewModel : IBasePostViewModel
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public ApplicationUser User { get; set; }

        public PostStatus Status { get; set; }

        [Required(ErrorMessage = "Title should not be empty.")]
        public string Title { get; set; }

        public string Content { get; set; }

        public PostTypes Type
        {
            get { return PostTypes.Post; }
            set { }
        }

        public int ParentId { get; set; }

        public DateTime DateUpdated { get; set; }

        public DateTime DateCreated { get; set; }

        public PostVisibility Visibility { get; set; }

        public string ModelType
        {
            get { return GetType().FullName; }
            set { }
        }
    }
}