﻿import React, {Component, PropTypes} from "react";
import ReactDOM from "react-dom";
import Cheers from "cheers-alert";

class ErrorContainer extends React.Component {
    render() {
        return (
            <ul>
                {this.props.errors.map(err => <li key={err}>{err}</li>)}
            </ul>
        )
    }
}

class FormContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            title: document.getElementById("SiteTitle").value,
            tagline: document.getElementById("SiteTagline").value,
            errors: []
        };
    }

    onTitleChange(event) {
        this.setState({ title: event.target.value });
    }

    onTaglineChange(event) {
        this.setState({ tagline: event.target.value });
    }

    onSubmit(event) {
        event.preventDefault();

        $.ajax({
            context: this,
            method: "post",
            url: document.getElementById("FormAction").value,
            data: {
                __RequestVerificationToken: document.querySelector("input[name='__RequestVerificationToken']").value,
                SiteTitle: this.state.title,
                SiteTagline: this.state.tagline
            },
            success: function(response) {
                if (!response.Errors) {
                    Cheers.info({
                        title: "Updated!",
                        message: "General settings updated.",
                        alert: "slideleft",
                        icon: "fa-exclamation",
                        duration: 3
                    });

                    this.setState({ errors: [] });
                } else {
                    Cheers.warning({
                        title: "Error!",
                        message: "Please see form errors.",
                        alert: "slideleft",
                        icon: "fa-times",
                        duration: 3
                    });

                    this.setState({ errors: response.Messages });
                }
            }
        });
    }

    render() {
        return (
            <div className="col-md-6">
                <form className="form-horizontal" onSubmit={this.onSubmit.bind(this)}>
                    <div className="box box-primary">
                        <div className="box-header">
                            <h3 className="box-title">General</h3>
                        </div>
                        <div className="box-body">
                            {this.state.errors.length > 0 && <ErrorContainer errors={this.state.errors} />}
                            <div className="form-group">
                                <label className="control-label col-md-3" htmlFor="form-title">Site Title</label>
                                <div className="col-md-9">
                                    <input id="form-title" className="form-control" name="title" type="text" value={this.state.title} onChange={this.onTitleChange.bind(this)}/>
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="control-label col-md-3" htmlFor="form-tagline">Site Tagline</label>
                                <div className="col-md-9">
                                    <input id="form-tagline" className="form-control" name="title" type="text" value={this.state.tagline} onChange={this.onTaglineChange.bind(this)}/>
                                </div>
                            </div>
                        </div>
                        <div className="box-footer">
                            <button className="btn btn-primary">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

ReactDOM.render(<FormContainer />, document.getElementById("form-container"))