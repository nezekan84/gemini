﻿using System;
using Gemini.Models;

namespace Gemini.Libraries.Models
{
    /// <summary>
    /// Since: Rev 7
    /// Current: Rev 11
    /// </summary>
    public class Settings
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public ApplicationUser User { get; set; }

        public string Entity { get; set; }

        public string Attribute { get; set; }

        public string Value { get; set; }

        public DateTime DateCreated { get; set; }
    }
}