﻿namespace Gemini.ViewModels.Categories
{
    public class CategoryParentToChildViewModel
    {
        public int ParentId { get; set; }

        public string ParentName { get; set; }

        public int ChildInt { get; set; }

        public string ChildName { get; set; }

        public int PostCount { get; set; }
    }
}