﻿using System.Data.Entity;
using Gemini.Libraries.Models;
using Gemini.Libraries.Models.Configurations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Gemini.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Posts> Posts { get; set; }

        public DbSet<PostMeta> PostMeta { get; set; }

        public DbSet<PostGroup> PostGroup { get; set; }

        public DbSet<Settings> Settings { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new PostsConfig());
            modelBuilder.Configurations.Add(new PostMetaConfig());
            modelBuilder.Configurations.Add(new PostGroupConfig());

            modelBuilder.Configurations.Add(new SettingsConfig());

            base.OnModelCreating(modelBuilder);
        }
    }
}