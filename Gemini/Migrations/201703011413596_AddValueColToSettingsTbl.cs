namespace Gemini.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddValueColToSettingsTbl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Settings", "Value", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Settings", "Value");
        }
    }
}
