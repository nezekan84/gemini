﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Gemini.Libraries.Components.ThemeEngine;
using Gemini.Libraries.Components.ThemeEngine.PageEngine;
using Gemini.Themes;

namespace Gemini
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var registrar = new ThemeRegistrar();
            registrar.Boot();

            // add theme view locations
            ViewEngines.Engines.Add(registrar.CurrentServiceProvider().WebForms);
            // add theme model binders
            AddModelBinders(registrar);

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }


        private void AddModelBinders(BaseThemeServiceEngine registrar)
        {
            foreach (var theme in registrar.Themes())
            {
                theme.Value.RegisterViewModels();
                var type = theme.Value.ViewModels.Page.GetType();

                if(!ModelBinders.Binders.Keys.Contains(type))
                    ModelBinders.Binders.Add(type, new PageModelBinder());
            }
        }
    }
}
