﻿using Gemini.Libraries.Components.ThemeEngine.PageEngine;

namespace Gemini.Libraries.Components.ThemeEngine
{
    /// <summary>
    /// View locations should manually be added view WebForms property. (ref: http://stackoverflow.com/a/7557275)
    /// 
    /// Since: Rev 10a
    /// Current: Rev 4
    /// </summary>
    public abstract class BaseThemeServiceProvider
    {
        /// <summary>
        /// Active flag. Whether current service provider is active.
        /// </summary>
        public bool Active;

        /// <summary>
        /// Set (custom) partial views here.
        /// </summary>
        public Views Views = new Views();

        /// <summary>
        /// Principal views models. You can override ViewModels here.
        /// </summary>
        public ViewModels ViewModels = new ViewModels();

        /// <summary>
        /// 
        /// </summary>
        public string QualifiedName;

        /// <summary>
        /// Theme image. (Will be shown on /Themes page)
        /// </summary>
        public string ThemeImg = "/Images/theme-placeholder.png";

        /// <summary>
        /// AUTO GENERATED!!!
        /// </summary>
        public readonly string ThemeName;

        /// <summary>
        /// 
        /// </summary>
        public WebForms WebForms = new WebForms();

        public BaseThemeServiceProvider()
        {
            QualifiedName = this.GetType().Name;
            ThemeName = QualifiedName.Replace("ThemeServiceProvider", null);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Reset()
        {
            Views.Reset();
            ViewModels.Reset();
        }

        /// <summary>
        /// Boot service provider
        /// </summary>
        public abstract void Boot();

        /// <summary>
        /// Register view models via ViewModels property.
        /// </summary>
        public abstract void RegisterViewModels();

        /// <summary>
        /// `
        /// </summary>
        public abstract void RegisterViews();
    }
}