﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Gemini.DataTransferObjects.Tag;
using Gemini.Libraries.Components;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.Enums;
using Gemini.Libraries.Models;
using Gemini.Models;
using Gemini.ViewModels.Tags;

namespace Gemini.Controllers
{
    /// <summary>
    /// Since: Rev ?a
    /// Current: Rev 4
    /// </summary>
    [Authorize]
    public class TagsController : Controller
    {
        private readonly ApplicationDbContext _context = new ApplicationDbContext();

        // GET: Tags
        public ViewResult Index()
        {
            return View(new TagListViewModel()
            {
                CreateUrl = Url.Action("Create"),
                DataUrl = Url.Action("TagData"),
                UpdateUrl = Url.Action("Update"),
                DeleteUrl = Url.Action("Delete")
            });
        }
    
        [OutputCache(Duration = 0)]
        public JsonResult TagData()
        {
            return Json(_context.PostGroup
                .Include(x => x.Posts)
                .Where(x => x.Type == PostGroupType.Tag)
                .OrderBy(x => x.Name)
                .Select(post => new TagItemDto()
                {
                    Id =  post.Id,
                    Description = post.Description,
                    Name = post.Name,
                    PostCount = post.Posts.Count,
                    Slug = post.Slug
                }));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Create(TagViewModel form)
        {
            bool isUnique =
                _context.PostGroup.FirstOrDefault(x => x.Type == PostGroupType.Tag && x.Name == form.Name) == null;

            if (!ModelState.IsValid || !isUnique)
            {
                List<string> errors = new List<string>();
                foreach (KeyValuePair<string, ModelState> item in ModelState)
                {
                    errors.AddRange(item.Value.Errors.Select(accessedErr => accessedErr.ErrorMessage));
                }

                if (!isUnique)
                    errors.Add("Tag should be unique.");

                return Json(new
                {
                    Errors = true,
                    Messages = errors
                });
            }

            _context.PostGroup.Add(new PostGroup()
            {
                Name = form.Name,
                Description = form.Description,
                Slug = form.Slug,
                Type = PostGroupType.Tag,
                DateCreated = DateTime.Now
            });
            _context.SaveChanges();

            return Json(new
            {
                Errors = false
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Update(TagViewModel form)
        {
            bool isUniqueToForm =
                _context.PostGroup.FirstOrDefault(
                    x => x.Type == PostGroupType.Tag && x.Name == form.Name && x.Id != form.Id) == null;

            if (ModelState.IsValid && isUniqueToForm)
            {
                PostGroup model = _context.PostGroup.FirstOrDefault(x => x.Id == form.Id);
                model.Name = form.Name;
                model.Slug = UrlSlugger.ToUrlSlug(form.Name);
                model.Description = form.Description;

                _context.SaveChanges();
                return Json(new { Errors = false });
            }

            List<string> errors = new List<string>();
            foreach (KeyValuePair<string, ModelState> item in ModelState)
            {
                var errs = item.Value.Errors;
                errors.AddRange(errs.Select(accessedErr => accessedErr.ErrorMessage));
            }

            if (!isUniqueToForm)
                errors.Add("Tag should be unique.");

            return Json(new { ErrCol = errors, Errors = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(TagListDeleteViewModel tagList)
        {
            try
            {
                int[] itemIds = tagList.Items.Select(x => x.Id).ToArray();
                List<PostGroup> items =
                    _context.PostGroup.Where(x => x.Type == PostGroupType.Tag && itemIds.Contains(x.Id)).ToList();
                _context.PostGroup.RemoveRange(items);
                _context.SaveChanges();

                return Json(new {Errors = false});
            }
            catch (Exception e)
            {
                return Json(new { Errors = true, Message = "An error has occurred."});
            }
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }
    }
}