﻿namespace Gemini.ViewModels.Tags
{
    public class TagListViewModel
    {
        public string DataUrl { get; set; }

        public string CreateUrl { get; set; }

        public string UpdateUrl { get; set; }

        public string DeleteUrl { get; set; }
    }
}