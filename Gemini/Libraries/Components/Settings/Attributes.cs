﻿namespace Gemini.Libraries.Components.Settings
{
    /// <summary>
    /// Since: Rev 19a
    /// Current: Rev 19a
    /// </summary>
    public class Attributes
    {
        //
        public static string SiteTagline => "site_tagline";

        // 
        public static string SiteTitle => "site_title";

        // attribute for site theme
        public static string Theme => "theme";
    }
}