﻿using System.Data.Entity.ModelConfiguration;

namespace Gemini.Libraries.Models.Configurations
{
    /// <summary>
    /// Since: Rev 11a
    /// Current: Rev 11a
    /// </summary>
    public class PostsConfig : EntityTypeConfiguration<Posts>
    {
        public PostsConfig()
        {
            ToTable("Posts");

            // columns
            Property(posts => posts.Content)
                .HasMaxLength(2000);
            Property(posts => posts.DateCreated)
                .HasColumnType("datetime2");
            Property(posts => posts.DateUpdated)
                .HasColumnType("datetime2");

            // relationships
            HasRequired(posts => posts.User)
                .WithMany(user => user.Posts)
                .HasForeignKey(posts => posts.UserId)
                .WillCascadeOnDelete(false);

            HasOptional(child => child.Parent)
                .WithMany(parent => parent.Children)
                .HasForeignKey(parent => parent.ParentId)
                .WillCascadeOnDelete(false);

            HasMany(posts => posts.Meta)
                .WithRequired(meta => meta.Post)
                .HasForeignKey(meta => meta.PostId);
        }
    }
}