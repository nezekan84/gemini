﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Gemini.DataTransferObjects.Category;
using Gemini.Libraries.Components;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.Enums;
using Gemini.Libraries.Models;
using Gemini.Models;
using Gemini.ViewModels.Categories;
using Newtonsoft.Json;

namespace Gemini.Controllers
{
    /// <summary>
    /// Since: Rev ?a
    /// Current: Rev 4
    /// </summary>
    [Authorize]
    public class CategoriesController : Controller
    {
        private readonly ApplicationDbContext _context = new ApplicationDbContext();

        /// <summary>
        /// Uncategorized category id. Deletion of this category is restricted.
        /// </summary>
        public const int Uncategorized = 1;

        public ViewResult Index()
        {
            return View(new CategoryListParamsViewModel()
            {
                FormAction = Url.Action("NewCategory"),
                TableUrl = Url.Action("List"),
                ModalDataUrl = Url.Action("RequestModalData"),
                UpdateAction = Url.Action("Update"),
                DeleteAction = Url.Action("Delete")
            });
        }

        [OutputCache(Duration = 0, NoStore = true)]
        public ContentResult List(int? page)
        {
            IQueryable<PostGroup> query = _context.PostGroup.Where(x => x.Type == PostGroupType.Category);
            List<PostGroup> cat = query
                .Include(x => x.Posts)
                .Include(x => x.Children.Select(y => y.Posts))
                .Where(x => x.ParentId == null)
                .OrderBy(x => x.Name)
                .ToList();

            List<CategoryViewModel> transform = new List<CategoryViewModel>();
            foreach (PostGroup catItem in cat)
            {
                transform.Add(new CategoryViewModel()
                {
                    Id = catItem.Id,
                    Name = catItem.Name,
                    Description = catItem.Description,
                    PostCount = catItem.Posts.Count
                });

                if (!catItem.Children.Any()) continue;

                var sortedChildren = catItem.Children.OrderBy(x => x.Name);
                transform.AddRange(sortedChildren.Select(subcat => new CategoryViewModel()
                {
                    Id = subcat.Id,
                    Name = subcat.Name,
                    Description = subcat.Description,
                    ParentId = subcat.Parent.Id,
                    ParentName = subcat.Parent.Name,
                    PostCount = subcat.Posts.Count
                }));
            }

            CategoryListViewModel viewModel = new CategoryListViewModel()
            {
                Categories = transform
            };

            var load = JsonConvert.SerializeObject(viewModel, Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            return Content(load);
        }

        [OutputCache(Duration = 0, NoStore = true)]
        public JsonResult RequestModalData()
        {
            List<CategoryDto> categories = _context.PostGroup
                .Where(x => x.Type == PostGroupType.Category && x.ParentId == null)
                .Select(cat => new CategoryDto()
                {
                    Id = cat.Id,
                    Name = cat.Name
                })
                .OrderBy(x => x.Name)
                .ToList();

            categories.Insert(0, new CategoryDto()
            {
                Id = 0,
                Name = "----------"
            });

            return Json(new ModalDataDto()
            {
                Categories = categories
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult NewCategory(CategoryViewModel form)
        {
            bool isUnique = _context.PostGroup
                .FirstOrDefault(x => x.Type == PostGroupType.Category && x.Name == form.Name) == null;

            if (!ModelState.IsValid || !isUnique)
            {
                List<string> errors = new List<string>();
                foreach (KeyValuePair<string, ModelState> item in ModelState)
                {
                    errors.AddRange(item.Value.Errors.Select(accessedErr => accessedErr.ErrorMessage));
                }

                if (!isUnique)
                    errors.Add("Category should be unique.");

                return Json(new { ErrCol = errors, Errors = true });
            }

            PostGroup newCategory = new PostGroup
            {
                Name = form.Name,
                Slug = UrlSlugger.ToUrlSlug(form.Name),
                Description = form.Description,
                DateCreated = DateTime.Now
            };

            // have to allow nullable so to allow create parent categories.
            if (form.ParentId > 0)
                newCategory.ParentId = form.ParentId;

            _context.PostGroup.Add(newCategory);
            _context.SaveChanges();
            return Json(new { Errors = false });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Update(CategoryViewModel form)
        {
            bool isUniqueToForm = _context.PostGroup.FirstOrDefault(x => x.Type == PostGroupType.Category && x.Id != form.Id && x.Name == form.Name) == null;

            if (ModelState.IsValid && isUniqueToForm)
            {
                PostGroup model = _context.PostGroup.SingleOrDefault(x => x.Id == form.Id);
                model.Name = form.Name;
                model.Slug = UrlSlugger.ToUrlSlug(form.Name);
                // it should NOT be able to set itself as its parent
                if (form.ParentId != null && form.ParentId != model.Id)
                    model.ParentId = form.ParentId;
                model.Description = form.Description;

                _context.SaveChanges();
                return Json(new { Errors = false });
            }

            List<string> errors = new List<string>();
            foreach (KeyValuePair<string, ModelState> item in ModelState)
            {
                var errs = item.Value.Errors;
                errors.AddRange(errs.Select(accessedErr => accessedErr.ErrorMessage));
            }

            if (!isUniqueToForm)
                errors.Add("Category should be unique.");

            return Json(new { ErrCol = errors, Errors = true });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Delete(CategoryDeleteListViewModel form)
        {
            try
            {
                // get every parent category
                int[] parentIds = form.Items
                    .Where(x => x.IsParent && x.Id != Uncategorized)
                    .Select(item => item.Id).ToArray();

                // move all oprhan categories to uncategorized.
                List<PostGroup> orphans =
                    _context.PostGroup
                    .Where(x => parentIds.Contains((int) x.ParentId)).ToList();
                foreach (PostGroup orphan in orphans)
                {
                    orphan.ParentId = Uncategorized;
                }

                // remove child categories
                int[] childIds = form.Items
                    .Where(x => !x.IsParent && x.Id != Uncategorized)
                    .Select(item => item.Id).ToArray();
                List<PostGroup> children = _context.PostGroup.Where(x => childIds.Contains(x.Id)).ToList();
                _context.PostGroup.RemoveRange(children);

                // finally, delete parent categories
                List<PostGroup> parents = _context.PostGroup.Where(x => parentIds.Contains(x.Id)).ToList();
                _context.PostGroup.RemoveRange(parents);
                _context.SaveChanges();

                return Json(new { Errors = false });
            }
            catch (Exception e)
            {
                return Json(new
                {
                    Errors = true,
                    Message = "An error has occurred. Please try again later."
                });
            }
        }

        protected override void Dispose(bool disposing)
        {
            _context.Dispose();
            base.Dispose(disposing);
        }
    }
}