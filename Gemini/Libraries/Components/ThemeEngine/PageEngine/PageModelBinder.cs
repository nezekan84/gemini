﻿using System;
using System.Web.Mvc;

namespace Gemini.Libraries.Components.ThemeEngine.PageEngine
{
    /// <summary>
    /// Since: Rev 13a
    /// Current: Rev 4
    /// </summary>
    public class PageModelBinder : DefaultModelBinder
    {
        protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
        {
            // get model type from form value
            ValueProviderResult typeValue = bindingContext.ValueProvider.GetValue("ModelType");

            // get Type of object
            Type type = Type.GetType(
                (string)typeValue.ConvertTo(typeof(string)),
                true
            );

            // create insstance of model and bind values
            object model = Activator.CreateInstance(type);

            bindingContext.ModelMetadata = ModelMetadataProviders.Current.GetMetadataForType(() => model, type);
            return model;
        }
    }
}