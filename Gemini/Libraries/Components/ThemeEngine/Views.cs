﻿using System.Reflection;

namespace Gemini.Libraries.Components.ThemeEngine
{
    /// <summary>
    /// Since: Rev 17a
    /// Current: Rev 22a
    /// </summary>
    public class Views
    {
        /// <summary>
        /// Relative directory to the partial view for page form. (/Pages/Create)
        /// </summary>
        public string Create;

        /// <summary>
        /// Relative directory to the partial viiew for page edit form. (/Pages/View)
        /// </summary>
        public string View;

        /// <summary>
        /// Reset values.
        /// </summary>
        public void Reset()
        {
            PropertyInfo[] props = GetType().GetProperties();

            foreach (PropertyInfo prop in props)
            {
                prop.SetValue(this, null);
            }
        }
    }
}