﻿namespace Gemini.DataTransferObjects.Tag
{
    public class TagItemDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Slug { get; set; }

        public string Description { get; set; }

        public int PostCount { get; set; }
    }
}