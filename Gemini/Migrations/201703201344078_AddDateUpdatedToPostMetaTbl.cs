namespace Gemini.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDateUpdatedToPostMetaTbl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PostMeta", "DateUpdated", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PostMeta", "DateUpdated");
        }
    }
}
