﻿using System.Collections.Generic;

namespace Gemini.ViewModels.Tags
{
    public class TagListDeleteViewModel
    {
        public List<TagListItemDeleteViewModel> Items { get; set; }
    }

    public class TagListItemDeleteViewModel
    {
        public int Id { get; set; }
    }
}