﻿using System;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.Contracts;
using Gemini.Libraries.Models;

namespace Gemini.Libraries.Components.ThemeEngine.PageEngine.PostViewModels
{
    /// <summary>
    /// Since: Rev 13a
    /// Current: Rev 4
    /// </summary>
    public class BasePostMetaViewModel : IBasePostMetaViewModel
    {
        public int Id { get; set; }

        public int PostId { get; set; }

        public Posts Post { get; set; }

        public string Meta { get; set; }

        public string Value { get; set; }

        public DateTime DateUpdated { get; set; }

        public DateTime DateCreated { get; set; }
    }
}