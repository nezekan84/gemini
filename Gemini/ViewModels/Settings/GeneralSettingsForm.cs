﻿using System.ComponentModel.DataAnnotations;

namespace Gemini.ViewModels.Settings
{
    public class GeneralSettingsForm
    {
        [Required(ErrorMessage = "Site Title is required.")]
        [StringLength(55, MinimumLength = 4, ErrorMessage = "Site Title should be between 4 to 55 characters long.")]
        [Display(Name = "Site Title")]
        public string SiteTitle { get; set; }

        [Display(Name = "Site Tagline")]
        public string SiteTagline { get; set; }
    }
}