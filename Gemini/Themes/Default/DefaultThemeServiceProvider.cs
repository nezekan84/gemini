﻿using Gemini.Libraries.Components.ThemeEngine;
using Gemini.Themes.Default.ViewModel.Pages;

namespace Gemini.Themes.Default
{
    public class DefaultThemeServiceProvider : BaseThemeServiceProvider
    {

        public override void Boot()
        {
            ThemeImg = "/Themes/Default/Images/placeholder.png";
        }

        public override void RegisterViewModels()
        {
            ViewModels.Page = new PageViewModel();
        }

        public override void RegisterViews()
        {
            Views.Create = "~/Themes/Default/Views/Pages/Form.cshtml";
            Views.View = "~/Themes/Default/Views/Pages/View.cshtml";

            WebForms.AddViewLocationFormat("~/Themes/Default/Views/{1}/{0}.cshtml");
            WebForms.AddPartialViewLocationFormat("~/Themes/Default/Views/{1}/{0}.cshtml");
        }
    }
}