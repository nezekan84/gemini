﻿using System.ComponentModel.DataAnnotations;

namespace Gemini.Libraries.Components.ThemeEngine.PageEngine.Enums
{
    /// <summary>
    /// Since: Rev 9a
    /// Current: Rev 4
    /// </summary>
    public enum PostVisibility
    {
        [Display(Name = "Public")]
        Public = 0,

        [Display(Name = "Private")]
        Private = 1
    }
}