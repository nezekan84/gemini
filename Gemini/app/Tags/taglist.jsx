﻿import React from "react";
import ReactDOM from "react-dom";
import {
    Modal as ReactModal,
    ModalHeader as ReactModalHeader,
    ModalTitle as ReactModalTitle,
    ModalBody as ReactModalBody,
    ModalFooter as ReactModalFooter,
    Button as ReactBtn,
    OverlayTrigger
} from "react-bootstrap";
import Cheers from "cheers-alert";
import { confirmAlert } from "react-confirm-alert";

class ModalErrorRow extends React.Component {
    render() {
        return <li>{this.props.message}</li>;
    }
}

class ModalErrors extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            errors: this.props.errors
        };
    }
    render() {
        return (
             <ul>
             {this.state.errors.map(function(i) {
                 return <ModalErrorRow message={i} key={Date.now()} />;
             })}
            </ul>
        );
    }
}

class TagComposer extends React.Component {
    constructor(props) {
        super(props);

        this.proceedDelete = this.proceedDelete.bind(this);
    }
    proceedDelete(checked) {
        if (!checked) {
            this.props.uncheckTag(this.props.tag.Id);
        } else {
            this.props.checkTag(this.props.tag.Id);
        }
    }
    render() {
        return (
            <tr>
                <td>
                    <input type="checkbox" onChange={this.proceedDelete} checked={this.props.isTagChecked(this.props.tag.Id)}/>
                </td>
                <td onClick={this.props.setEditMode.bind(null, this.props.tag)}>{this.props.tag.Name}</td>
                <td>{this.props.tag.PostCount}</td>
            </tr>
        );
    }
}

class TagTableContainer extends React.Component {
    constructor(props) {
        super(props);

        this.onDelete = this.onDelete.bind(this);
    }
    onDelete() { 
        if (this.props.queueDelete().length > 0) {
            $.ajax({
                context:this,
                url: document.getElementById("DeleteUrl").value,
                method: "post",
                data: {
                    __RequestVerificationToken: document.querySelector("[name='__RequestVerificationToken']").value,
                    Items: this.props.queueDelete()
                },
                success: function(response) {
                    if (response.Errors) {
                        Cheers.warning({
                            title: "An Error Has Occurred!",
                            message: response.Message,
                            alert: "slideleft",
                            icon: "fa-times",
                            duration: 3
                        });
                    } else {
                        this.props.refreshList();
                        this.props.resetQueueDelete();
                        Cheers.info({
                            title: "Updated!",
                            message: "Tag list updated.",
                            alert: "slideleft",
                            icon: "fa-exclamation",
                            duration: 3
                        });
                    }
                    this.props.unsetEditMode();
                }
            });
        }
    }
    render() {
        return (
          <table className="table table-hover">
              <thead>
                   <tr>
                       <th>
                           <input id="chk-delall-top" type="checkbox" onChange={this.props.allTagSelector} />
                           <button className="btn btn-primary" onClick={this.onDelete}>Delete</button>
                       </th>
                       <th>Name</th>
                       <th>Post Count</th>
                   </tr>
              </thead>
              <tbody>
                  {this.props.tags.map(function(data) {
                      return <TagComposer
                          tag={data}
                          key={data.Name + this.props.isTagChecked(data.Id).toString()}
                          setEditMode={this.props.setEditMode}
                          checkTag={this.props.checkTag}
                          uncheckTag={this.props.uncheckTag}
                          isTagChecked={this.props.isTagChecked}/>
                      }, this)}
              </tbody>
              <tfoot>
                  <tr>
                       <th>
                           <input id="chk-delall-bot" type="checkbox" onChange={this.props.allTagSelector} />
                           <button className="btn btn-primary" onClick={this.onDelete}>Delete</button>
                       </th>
                       <th>Name</th>
                       <th>Post Count</th>
                   </tr>
              </tfoot>
          </table>
        );
    }
}

class EditFormContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            id: this.props.tag.Id,
            name: this.props.tag.Name,
            description: this.props.tag.Description
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.onNameChange = this.onNameChange.bind(this);
        this.onDescChange = this.onDescChange.bind(this);
        this.notify = this.notify.bind(this);
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            id: nextProps.tag.Id,
            name: nextProps.tag.Name,
            description: nextProps.tag.Description
        });
    }
    onNameChange(e) {
        this.setState({ name: e.target.value });
    }
    onDescChange(e) {
        this.setState({ description: e.target.value });
    }
    onSubmit(e) {
        e.preventDefault();

        $.ajax({
            context: this,
            url: document.getElementById("UpdateUrl").value,
            method: "post",
            data: {
                __RequestVerificationToken: document.querySelector("[name='__RequestVerificationToken']").value,
                Id: this.state.id,
                Name: this.state.name,
                Description: this.state.description
            },
            success: function(response) {
                if (!response.Errors) {
                    this.props.refreshList();
                    this.props.unsetEditMode();
                    this.notify("Updated!", "You have updated a category.", "fa-exclamation", "info");
                } else {
                    this.notify("Error!", response.ErrCol, "fa-times", "error");
                    this.setState({ errors: response.ErrCol });
                }
            }
        });
    }
    notify(title, message, icon, type = "info") {
        switch(type) {
            case "error":
                Cheers.error({
                    title: title,
                    message: message,
                    alert: "slideleft",
                    icon: icon,
                    duration: 3
                });
                break;
            case "info":
            default:
                Cheers.info({
                    title: title,
                    message: message,
                    alert: "slideleft",
                    icon: icon,
                    duration: 3
                });
                break;
        }
    }
    render() {
        return (
            <div className="col-md-4">
                <form className="form-horizontal" onSubmit={this.onSubmit}>
                    <div className="box box-primary">
                        <div className="box-header">
                            <h3 className="box-title">Edit Tag</h3>
                            <button type="button" className="close" aria-label="Close" onClick={this.props.unsetEditMode}>
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div className="box-body">
                            <input type="hidden" id="edit-id" value={this.state.id} />
                            <div className="form-group">
                                <label className="control-label col-md-2" htmlFor="edit-name">Name</label>
                                <div className="col-md-10">
                                    <input className="form-control" id="edit-name" type="text" value={this.state.name || ""} onChange={this.onNameChange}/>
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="control-label col-md-2" htmlFor="edit-name">Description</label>
                                <div className="col-md-10">
                                    <input className="form-control" id="edit-desc" type="text" value={this.state.description || ""} onChange={this.onDescChange}/>
                                </div>
                            </div>
                        </div>
                        <div className="box-footer">
                            <button className="btn btn-default" onClick={this.props.unsetEditMode}>Cancel</button>
                            <button className="btn btn-primary pull-right" type="submit">Update</button>
                        </div>
                    </div>
                </form>
            </div>
            );
    }
}

class OuterTableContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            errors: [],
            show: false,
            name: "",
            description: ""
        };

        this.onHide = this.onHide.bind(this);
        this.onShow = this.onShow.bind(this);
        this.onNameChange = this.onNameChange.bind(this);
        this.onDescChange = this.onDescChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    onDescChange(e) {
        this.setState({ description: e.target.value });
    }
    onNameChange(e) {
        this.setState({ name: e.target.value });
    }
    onHide() {
        this.setState({
            show: false,
            errors: [],
            name: "",
            description: ""
        });
    }
    onShow() {
        this.setState({ show: true });
    }
    onSubmit(e) {
        e.preventDefault();

        $.ajax({
            context: this,
            url: document.getElementById("CreateUrl").value,
            method: "post",
            data: {
                __RequestVerificationToken: document.querySelector("[name='__RequestVerificationToken']").value,
                Name: this.state.name,
                Description: this.state.description
            },
            success: function(response) {
                if (response.Errors) {
                    this.setState({ errors: response.Messages });
                } else {
                    this.props.refreshList();
                    this.onHide();
                    Cheers.success({
                        title: "Tag Created!",
                        message: "You have created a new tag.",
                        alert: "slideleft",
                        icon: "fa-check",
                        duration: 3
                    });
                }
            }
        });
    }
    render() {
        var errs = null;
        if(this.state.errors.length > 0) {
            errs = <ModalErrors errors={this.state.errors}/>;
        }
        return (
            <div className="box box-primary">
                <div className="box-header">
                    <h3 className="box-title">Tags</h3>
                    <ReactBtn bsStyle="primary" className="pull-right" onClick={this.onShow}>Add New</ReactBtn>
                </div>
                <div className="box-body table-responsive">
                    <TagTableContainer
                        tags={this.props.tags}
                        setEditMode={this.props.setEditMode}
                        unsetEditMode={this.props.unsetEditMode}
                        refreshList={this.props.refreshList}
                        resetQueueDelete={this.props.resetQueueDelete}
                        queueDelete={this.props.queueDelete}
                        allTagSelector={this.props.allTagSelector}
                        checkTag={this.props.checkTag}
                        uncheckTag={this.props.uncheckTag}
                        isTagChecked={this.props.isTagChecked} />
    </div>
    <ReactModal id="new-catg-modal" show={this.state.show} onHide={this.onHide}>
        <form className="form-horizontal" method="post" onSubmit={this.onSubmit}>
            <ReactModalHeader closeButton>
                <ReactModalTitle>New Tag</ReactModalTitle>
            </ReactModalHeader>
            <ReactModalBody>
                {errs}
                <div className="form-group">
                    <label className="control-label col-md-2" htmlFor="cat-name">Name</label>
                    <div className="col-md-10">
                        <input className="form-control" id="cat-name" name="Name" type="text" onChange={this.onNameChange} value={this.state.name} />
                    </div>
                </div>
                <div className="form-group">
                    <label className="control-label col-md-2" htmlFor="cat-desc">Description</label>
                    <div className="col-md-10">
                        <input className="form-control" id="cat-desc" name="Description" type="text" onChange={this.onDescChange} value={this.state.description}/>
                    </div>
                </div>
            </ReactModalBody>
            <ReactModalFooter>
                <ReactBtn bsStyle="primary" className="pull-right" type="submit">Submit</ReactBtn>
            </ReactModalFooter>
        </form>
    </ReactModal>
</div>
            );
    }
}

class TagListContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            edit: false,
            tags: [],
            selectedTag: {},
            checkedTags: []
        };

        this.refreshList = this.refreshList.bind(this);
        this.setEditMode = this.setEditMode.bind(this);
        this.unsetEditMode = this.unsetEditMode.bind(this);
        this.unsetEditModeKeyPress = this.unsetEditModeKeyPress.bind(this);
        this.resetQueueDelete = this.resetQueueDelete.bind(this);
        this.queueDelete = this.queueDelete.bind(this);
        this.allTagSelector = this.allTagSelector.bind(this);
        this.checkTag = this.checkTag.bind(this);
        this.uncheckTag = this.uncheckTag.bind(this);
        this.isTagChecked = this.isTagChecked.bind(this);
    }
    resetQueueDelete() {
        document.getElementById("chk-delall-top").checked = false;
        document.getElementById("chk-delall-bot").checked = false;
        this.setState({ selectedTag: [] });
    }
    queueDelete() {
        return this.state.checkedTags;
    }
    allTagSelector(event) {
        event.persist();
        var checked = event.target.checked;

        if (checked) {
            confirmAlert({
                title: "Warning",
                message: "You are attempting to delete all tags. Are you sure you want to continue?",
                confirmLabel: "Confirm",
                cancelLabel: "Cancel",
                onConfirm: () => {
                    document.getElementById("chk-delall-top").checked = true;
                    document.getElementById("chk-delall-bot").checked = true;
                    var col = [];
                    $.each(this.state.tags, function(i, tag) {
                        col.push({Id: tag.Id});
                    });
                    this.setState({ checkedTags: col });
                },
                onCancel: () => {
                    document.getElementById("chk-delall-top").checked = false;
                    document.getElementById("chk-delall-bot").checked = false;
                }
            });
        } else {
            this.resetQueueDelete();
        }
    }
    checkTag(id) {
        var col = this.state.checkedTags;

        if (col.findIndex(x => x.Id === id) === -1) {
            col.push({Id: id});
            this.setState({ checkedTags: col });
        }
    }
    uncheckTag(id) {
        var index = this.state.checkedTags.findIndex(x => x.Id === id);

        if (index > -1) {
            var col = this.state.checkedTags;
            col.splice(index, 1);
            this.setState({ checkedTags: col });
        }
    }
    isTagChecked(id) {
        return this.state.checkedTags.findIndex(x => x.Id === id) > -1;
    }
    refreshList() {
        $.ajax({
            cache: false,
            context: this,
            url: document.getElementById("DataUrl").value,
            method: "post",
            success: function(response) {
                this.setState({ tags: response });
            }
        });
    }
    setEditMode(tag) {
        this.setState({ edit: true, selectedTag: tag });
    }
    unsetEditMode() {
        this.setState({ edit: false });
    }
    unsetEditModeKeyPress(e) {
        if (e.keyCode === 27) {
            this.setState({ edit: false });
        }
    }
    componentWillMount() {
        this.refreshList();
        document.addEventListener("keydown", this.unsetEditModeKeyPress);
    }
    componentWillUnmount() {
        document.removeEventListener("keydown", this.unsetEditModeKeyPress);
    }
    render() {
        var containerCls = (this.state.edit) ? "col-md-8 table-responsive" : "col-md-12 table-responsive";
        return (
            <div>
                <div className={containerCls}>
                    <OuterTableContainer tags={this.state.tags}
                                     setEditMode={this.setEditMode}
                                     unsetEditMode={this.unsetEditMode}
                                     refreshList={this.refreshList}
                                     resetQueueDelete={this.resetQueueDelete}
                                     queueDelete={this.queueDelete}
                                     allTagSelector={this.allTagSelector}
                                     checkTag={this.checkTag}
                                     uncheckTag={this.uncheckTag}
                                     isTagChecked={this.isTagChecked} />
                </div>
                {this.state.edit && <EditFormContainer
                                                       unsetEditMode={this.unsetEditMode}
                                                       tag={this.state.selectedTag}
                                                       refreshList={this.refreshList}
                                                       />}
            </div>
        )
    }
}

ReactDOM.render(<TagListContainer />, document.getElementById("tag-view-container"))