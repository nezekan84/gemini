namespace Gemini.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDateUpdatedColToPostsTbl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Posts", "DateUpdated", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Posts", "DateUpdated");
        }
    }
}
