﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Gemini.Libraries.Components.Settings;
using Gemini.ViewModels.Settings;

namespace Gemini.Controllers
{
    /// <summary>
    /// Since: Rev 5a
    /// Current: Rev 4
    /// </summary>
    [Authorize]
    public class SettingsController : Controller
    {
        private readonly SettingsComponent _settingsComponent = new SettingsComponent();

        public ViewResult General()
        {
            GeneralSettingsFormData vm = new GeneralSettingsFormData()
            {
                FormAction = Url.Action("UpdateGeneral"),
                SiteTitle = _settingsComponent.Get(Entities.Global, Attributes.SiteTitle),
                SiteTagline = _settingsComponent.Get(Entities.Global, Attributes.SiteTagline)
            };

            return View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateGeneral(GeneralSettingsForm form)
        {
            if (ModelState.IsValid)
            {
                _settingsComponent.Set(Entities.Global, Attributes.SiteTitle, form.SiteTitle, true);
                _settingsComponent.Set(Entities.Global, Attributes.SiteTagline, form.SiteTagline, true);

                return Json(new { Errors = false });
            }

            List<string> errors = new List<string>();
            foreach (KeyValuePair<string, ModelState> item in ModelState)
            {
                errors.AddRange(item.Value.Errors.Select(accessedErr => accessedErr.ErrorMessage));
            }

            return Json(new { Errors = true, Messages = errors });
        }
    }
}