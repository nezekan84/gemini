﻿using System.Web.Mvc;
using Gemini.Libraries.ViewModels.Themes;
using Gemini.Themes;

namespace Gemini.Controllers
{
    /// <summary>
    /// Since: Rev 7a
    /// Current: Rev 4
    /// </summary>
    [Authorize]
    public class AppearanceController : Controller
    {
        private readonly ThemeRegistrar _themeEngine = new ThemeRegistrar();
        
        public ActionResult Index()
        {
            ThemeListViewModel vm = new ThemeListViewModel()
            {
                Themes = _themeEngine.Themes()
            };

            return View(vm);
        }

        [Route("themes/activate/{themeName}")]
        public ActionResult ActivateTheme(string themeName)
        {
            _themeEngine.ActivateTheme(themeName);
            return RedirectToAction("Index");
        }
    }
}