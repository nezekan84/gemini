﻿using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.PostViewModels;

namespace Gemini.Libraries.Components.ThemeEngine.Helpers
{
    /// <summary>
    /// Since: Rev 4
    /// Current: Rev 5
    /// </summary>
    public static class ThemeHelper
    {
        public static IHtmlString MetaLabel(this HtmlHelper helper, string meta)
        {
            return CreateMetaLabel(helper, meta, new { name = meta }, meta);
        }

        public static IHtmlString MetaLabel(this HtmlHelper helper, string meta, object htmlOptions)
        {
            return CreateMetaLabel(helper, meta, htmlOptions);
        }

        public static IHtmlString MetaLabel(this HtmlHelper helper, string meta, string id, object htmlOptions)
        {
            return CreateMetaLabel(helper, meta, htmlOptions, id);
        }

        private static IHtmlString CreateMetaLabel(HtmlHelper helper, string meta, object htmlOptions = null, string id = null)
        {
            BasePostMetaViewModel metaViewModel = EngineState.Instance
                .ServiceProvider
                .ViewModels
                .Page
                .Meta
                .SingleOrDefault(x => x.Meta == meta);
            int index = EngineState.Instance.ServiceProvider.ViewModels.Page.Meta.IndexOf(metaViewModel);

            TagBuilder labelBuilder = new TagBuilder("label");
            labelBuilder.InnerHtml = metaViewModel.Meta;
            labelBuilder.Attributes.Add("for", $"Meta[{index}].Value");

            if (htmlOptions != null)
                PushAttributes(labelBuilder, htmlOptions);

            return new MvcHtmlString($"{labelBuilder}");
        }

        public static IHtmlString MetaInput(this HtmlHelper helper, string meta)
        {
            return CreateMetaInput(helper, meta);
        }

        public static IHtmlString MetaInput(this HtmlHelper helper, string meta, object htmlOptions)
        {
            return CreateMetaInput(helper, meta, htmlOptions: htmlOptions);
        }

        public static IHtmlString MetaInput(this HtmlHelper helper, string meta, string id, object htmlOptions)
        {
            return CreateMetaInput(helper, meta, id, htmlOptions);
        }

        private static IHtmlString CreateMetaInput(HtmlHelper helper, string meta, string id = null, object htmlOptions = null)
        {
            BasePageViewModel pageViewModel = (BasePageViewModel)EngineState.Instance.ViewModelStates.Get(EngineState.Instance.ServiceProvider.ViewModels.Page);
            BasePostMetaViewModel metaViewModel = pageViewModel.Meta.Find(x => x.Meta == meta);
            int index = pageViewModel.Meta.IndexOf(metaViewModel);

            TagBuilder inputBuilder = new TagBuilder("input");
            inputBuilder.Attributes.Add("id", $"Meta[{index}].Value");

            if (htmlOptions != null)
                PushAttributes(inputBuilder, htmlOptions);

            inputBuilder.Attributes.Add("name", $"Meta[{index}].Value");
            inputBuilder.Attributes.Add("value", metaViewModel.Value);
            return new MvcHtmlString($"{inputBuilder}");
        }

        public static IHtmlString MetaHidden(this HtmlHelper helper, string meta)
        {
            BasePageViewModel pageViewModel = (BasePageViewModel)EngineState.Instance.ViewModelStates.Get(EngineState.Instance.ServiceProvider.ViewModels.Page);
            BasePostMetaViewModel metaViewModel = pageViewModel.Meta.Find(x => x.Meta == meta);
            int index = pageViewModel.Meta.IndexOf(metaViewModel);

            TagBuilder hiddenbuilder = new TagBuilder("input");
            hiddenbuilder.Attributes.Add("type", "hidden");
            hiddenbuilder.Attributes.Add("name", $"Meta[{index}].Meta");
            hiddenbuilder.Attributes.Add("value", metaViewModel.Meta);
            return new MvcHtmlString($"{hiddenbuilder}");
        }

        private static void PushAttributes(TagBuilder builder, object htmlOptions)
        {
            PropertyInfo[] options = htmlOptions.GetType().GetProperties();

            foreach (PropertyInfo option in options)
            {
                builder.Attributes.Add(option.Name, option.GetValue(htmlOptions).ToString());
            }
        }
    }
}