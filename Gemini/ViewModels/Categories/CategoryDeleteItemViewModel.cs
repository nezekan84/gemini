﻿namespace Gemini.ViewModels.Categories
{
    public class CategoryDeleteItemViewModel
    {
        public int Id { get; set; }

        public int? Parent { get; set; }

        public bool IsParent { get; set; }
    }
}