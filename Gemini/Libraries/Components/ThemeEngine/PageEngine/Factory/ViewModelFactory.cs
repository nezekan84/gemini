﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.Contracts;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.PostViewModels;

namespace Gemini.Libraries.Components.ThemeEngine.PageEngine.Factory
{
    /// <summary>
    /// Since: Rev 17a
    /// Current: Rev 4
    /// </summary>
    public class ViewModelFactory
    {
        private Dictionary<Type ,IViewModelFactoryItem> Collection { get; } = new Dictionary<Type, IViewModelFactoryItem>();

        public ViewModelFactory()
        {
            Init();
        }

        /// <summary>
        /// Set system view models.
        /// </summary>
        private void Init()
        {
            Collection.Add(new BasePageViewModel().GetType(), new BasePageViewModel());
            Collection.Add(new BasePostMetaViewModel().GetType(), new BasePostMetaViewModel());
            Collection.Add(new PostViewModel().GetType(), new PostViewModel());
            Collection.Add(new RevisionViewModel().GetType(), new RevisionViewModel());
            Collection.Add(new WidgetViewModel().GetType(), new WidgetViewModel());
        }

        /// <summary>
        /// Add new view model to factory.
        /// </summary>
        /// <param name="viewModel"></param>
        public void Add(IViewModelFactoryItem viewModel)
        {
            Collection.Add(viewModel.GetType(), viewModel);
        }

        /// <summary>
        /// Get view model by type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public IViewModelFactoryItem Get(Type type)
        {
            try
            {
                Type itemType = Collection.SingleOrDefault(x => x.Key == type).Value.GetType();
                return (IViewModelFactoryItem)Activator.CreateInstance(itemType);
            }
            catch (Exception e)
            {
                Exception exc = (Exception) Activator.CreateInstance(e.GetType(), $"ViewModelFactory does not have Type of: <{type.FullName}>.", e);
                throw exc;
            }
        }

        /// <summary>
        /// Reset factory.
        /// </summary>
        public void Reset()
        {
            Collection.Clear();
            Init();
        }
    }
}