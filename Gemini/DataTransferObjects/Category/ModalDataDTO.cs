﻿using System.Collections.Generic;

namespace Gemini.DataTransferObjects.Category
{
    public class ModalDataDto
    {
        public List<CategoryDto> Categories { get; set; }
    }
}