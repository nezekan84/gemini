﻿using System.Collections.Generic;
using Gemini.Libraries.Components.ThemeEngine;

namespace Gemini.Libraries.ViewModels.Themes
{
    /// <summary>
    /// Since: Rev 9
    /// Current: Rev 11
    /// </summary>
    public class ThemeListViewModel
    {
        public IDictionary<string, BaseThemeServiceProvider> Themes;
    }
}