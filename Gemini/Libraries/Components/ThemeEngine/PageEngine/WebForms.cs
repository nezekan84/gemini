﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Gemini.Libraries.Components.ThemeEngine.PageEngine
{
    /// <summary>
    /// Since: Rev 13a
    /// Current: Rev 4
    /// </summary>
    public class WebForms : RazorViewEngine
    {
        /// <summary>
        /// Add view location
        /// </summary>
        /// <param name="path"></param>
        public void AddViewLocationFormat(string path)
        {
            List<string> paths = new List<string>(ViewLocationFormats);
            paths.Add(path);

            ViewLocationFormats = paths.ToArray();
        }

        /// <summary>
        /// Add parti view location
        /// </summary>
        /// <param name="path"></param>
        public void AddPartialViewLocationFormat(string path)
        {
            List<string> paths = new List<string>(PartialViewLocationFormats);
            paths.Add(path);

            PartialViewLocationFormats = paths.ToArray();
        }
    }
}