namespace Gemini.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenamePostPK : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PostMeta", "PostId", "dbo.Posts");
            DropForeignKey("dbo.Posts", "ParentId", "dbo.Posts");
            DropPrimaryKey("dbo.Posts");
            RenameColumn("dbo.Posts", "Id", "PostId");
//            AddColumn("dbo.Posts", "PostId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Posts", "PostId");
            AddForeignKey("dbo.PostMeta", "PostId", "dbo.Posts", "PostId", cascadeDelete: true);
            AddForeignKey("dbo.Posts", "ParentId", "dbo.Posts", "PostId");
//            DropColumn("dbo.Posts", "Id");
        }
        
        public override void Down()
        {
//            AddColumn("dbo.Posts", "Id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.Posts", "ParentId", "dbo.Posts");
            DropForeignKey("dbo.PostMeta", "PostId", "dbo.Posts");
            DropPrimaryKey("dbo.Posts");
            RenameColumn("dbo.Posts", "PostId", "Id");
//            DropColumn("dbo.Posts", "PostId");
            AddPrimaryKey("dbo.Posts", "Id");
            AddForeignKey("dbo.Posts", "ParentId", "dbo.Posts", "Id");
            AddForeignKey("dbo.PostMeta", "PostId", "dbo.Posts", "Id", cascadeDelete: true);
        }
    }
}
