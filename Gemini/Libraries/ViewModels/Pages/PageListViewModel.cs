﻿using Gemini.Themes.Default.ViewModel.Pages;
using PagedList;

namespace Gemini.Libraries.ViewModels.Pages
{
    /// <summary>
    /// Since: Rev ?a
    /// Current: Rev ?a
    /// </summary>
    public class PageListViewModel
    {
        public StaticPagedList<PageViewModel> Pages { get; set; }
    }
}