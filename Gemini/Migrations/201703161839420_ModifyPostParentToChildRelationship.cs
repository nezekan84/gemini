namespace Gemini.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifyPostParentToChildRelationship : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Posts", new[] { "ParentId" });
            AlterColumn("dbo.Posts", "ParentId", c => c.Int());
            CreateIndex("dbo.Posts", "ParentId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Posts", new[] { "ParentId" });
            AlterColumn("dbo.Posts", "ParentId", c => c.Int(nullable: false));
            CreateIndex("dbo.Posts", "ParentId");
        }
    }
}
