﻿using System.ComponentModel.DataAnnotations;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.PostViewModels;

namespace Gemini.Themes.Default.ViewModel.PageMeta
{
    public class TestAttr2 : BasePostMetaViewModel
    {
        public TestAttr2()
        {
            Meta = "testAttr2";
        }

        [Display(Name="TEST META 2")]
//        [DisplayName("TEST META 2")]
        public new string Value { get; set; }
    }
}