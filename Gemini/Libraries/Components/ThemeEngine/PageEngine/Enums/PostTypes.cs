﻿using System.ComponentModel.DataAnnotations;

namespace Gemini.Libraries.Components.ThemeEngine.PageEngine.Enums
{
    /// <summary>
    /// Since: Rev 8a
    /// Current: Rev 4
    /// </summary>
    public enum PostTypes
    {
        [Display(Name = "Page")]
        Page = 0,

        [Display(Name = "Post")]
        Post = 1,

        [Display(Name = "Revision")]
        Revision = 2,

        [Display(Name = "Widget")]
        Widget = 3
    }
}