﻿using System;
using System.Collections.Generic;
using Gemini.Libraries.Components.Settings;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.PostViewModels;
using Gemini.Themes.Default;

namespace Gemini.Libraries.Components.ThemeEngine
{
    /// <summary>
    /// Qualified theme name = Theme name with ThemeServiceProvider suffix.
    /// Theme name = theme name without ThemeServiceProvider suffix.
    /// 
    /// Since: Rev 7a
    /// Current: Rev 5
    /// </summary>
    public abstract class BaseThemeServiceEngine
    {
        /// <summary>
        /// SettingsComponent instance.
        /// </summary>
        protected SettingsComponent SettingsComponent = new SettingsComponent();

        /// <summary>
        /// EngineState instance.
        /// </summary>
        private readonly EngineState _engineState = EngineState.Instance;

        /// <summary>
        /// Boot theme engine.
        /// </summary>
        public abstract void Boot();

        /// <summary>
        /// Register theme provider.
        /// </summary>
        /// <param name="serviceProvider"></param>
        protected void Add(BaseThemeServiceProvider serviceProvider)
        {
            _engineState.ServiceProviders.Add(serviceProvider.GetType().Name, serviceProvider);
        }

        /// <summary>
        /// Initialize theme engine.
        /// </summary>
        public void Initialize()
        {
            _engineState.ActiveTheme = SettingsComponent.Get(Entities.Global, Attributes.Theme);

            if (_engineState.ActiveTheme != null)
            {
                // get current theme provider
                _engineState.ServiceProviderName = ServiceProviderName(_engineState.ActiveTheme);
                _engineState.ServiceProvider = GetProvider(_engineState.ServiceProviderName);
                _engineState.ActiveTheme = _engineState.ServiceProvider.ThemeName;
            }
            else
            {
                // get default service provider
                _engineState.ServiceProvider = _engineState.ServiceProviders[typeof(DefaultThemeServiceProvider).Name];
            }

            // mark every inactive service provider as Active = false
            foreach (var xTheme in _engineState.ServiceProviders)
            {
                xTheme.Value.Active = (xTheme.Value.ThemeName == _engineState.ActiveTheme);

                if (xTheme.Value.ThemeName != _engineState.ActiveTheme)
                {
                    // manual garbage collection. (specially after switching themes.)
                    xTheme.Value.Reset();
                }
            }
            
            ProcCurrentServiceProvider();    
        }

        /// <summary>
        /// Process current service provider.
        /// </summary>
        private void ProcCurrentServiceProvider()
        {
            // mark service provider as active
            _engineState.ServiceProvider.Active = true;
            // boot service provider
            _engineState.ServiceProvider.Boot();
            // register views & view models
            _engineState.ServiceProvider.RegisterViews();
            _engineState.ServiceProvider.RegisterViewModels();

            // reset factory and register new view models.
            _engineState.ViewModelFactory.Reset();
            _engineState.ViewModelFactory.Add(_engineState.ServiceProvider.ViewModels.Page);
        }

        /// <summary>
        /// Get theme provider instance.
        /// </summary>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public BaseThemeServiceProvider GetProvider(string serviceProvider)
        {
            try
            {
                return _engineState.ServiceProviders[serviceProvider];
            }
            catch (Exception)
            {
                throw new NotImplementedException($"{serviceProvider} is not implemented");
            }
        }

        /// <summary>
        /// Get registered themes.
        /// </summary>
        /// <returns></returns>
        public SortedDictionary<string, BaseThemeServiceProvider> Themes()
        {
            return _engineState.ServiceProviders;
        }

        /// <summary>
        /// Register theme by Theme name NOT qualified name.
        /// </summary>
        /// <param name="themeName"></param>
        public int ActivateTheme(string themeName)
        {
            int set = SettingsComponent.Set(Entities.Global, Attributes.Theme, themeName, true);

            if (set > 0)
                Initialize();

            return set;
        }

        /// <summary>
        /// Get service provider class name.
        /// </summary>
        /// <param name="themeName"></param>
        /// <returns></returns>
        public string ServiceProviderName(string themeName)
        {
            return $"{themeName}ThemeServiceProvider";
        }

        /// <summary>
        /// Get view model for /Pages/Create.
        /// </summary>
        /// <returns></returns>
        public BasePageViewModel GetPageViewModel()
        {
            // proxy isnt needed here anyway.
            BasePageViewModel viewModel = (BasePageViewModel)_engineState.ViewModelFactory.Get(_engineState.ServiceProvider.ViewModels.Page.GetType());

            viewModel.FormPartial = _engineState.ServiceProvider.Views.Create;

            return viewModel;
        }

        /// <summary>
        /// Get view model for /Pages/View.
        /// </summary>
        /// <returns></returns>
        public BasePageViewModel GetEditPageViewModel()
        {
            BasePageViewModel viewModel = (BasePageViewModel) _engineState.ViewModelFactory.Get(_engineState.ServiceProvider.ViewModels.Page.GetType());

            if (EngineState.Instance.ViewModelStates.Exists(viewModel))
                return (BasePageViewModel) EngineState.Instance.ViewModelStates.Get(viewModel);

            viewModel.FormPartial = _engineState.ServiceProvider.Views.View;
            EngineState.Instance.ViewModelStates.Push(viewModel);

            return viewModel;
        }

        public BaseThemeServiceProvider CurrentServiceProvider()
        {
            return _engineState.ServiceProvider;
        }
    }
}