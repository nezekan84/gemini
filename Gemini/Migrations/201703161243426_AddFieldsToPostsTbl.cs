namespace Gemini.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldsToPostsTbl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Posts", "PostStatus", c => c.Int(nullable: false));
            AddColumn("dbo.Posts", "Visibility", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Posts", "Visibility");
            DropColumn("dbo.Posts", "PostStatus");
        }
    }
}
