﻿using System.ComponentModel.DataAnnotations;

namespace Gemini.ViewModels.Tags
{
    public class TagViewModel
    {
        public int Id { get; set; }

        [StringLength(55, MinimumLength = 4, ErrorMessage = "Tag should be between 4 to 55 characters long.")]
        public string Name { get; set; }

        public string Slug { get; set; }

        public string Description { get; set; }
    }
}