namespace Gemini.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInitialCategory : DbMigration
    {
        public override void Up()
        {
            DateTime now = DateTime.Now;
            Sql($"SET IDENTITY_INSERT [dbo].[PostGroup] ON\r\nINSERT INTO [dbo].[PostGroup] ([Id], [Type], [Name], [ParentId], [DateCreated], [Slug]) VALUES (1, 0, N\'Uncategorized\', NULL, N\'{now}\', N\'Uncategorized\')\r\nSET IDENTITY_INSERT [dbo].[PostGroup] OFF");
        }

        public override void Down()
        {
            Sql("DELETE FROM [dbo].[PostGroup] WHERE Id = 1");
        }
    }
}
