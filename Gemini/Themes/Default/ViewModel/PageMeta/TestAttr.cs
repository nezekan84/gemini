﻿using System.ComponentModel.DataAnnotations;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.PostViewModels;

namespace Gemini.Themes.Default.ViewModel.PageMeta
{
    public class TestAttr : BasePostMetaViewModel
    {
        public TestAttr()
        {
            Meta = "testAttr";
        }

        [Display(Name="SOMEATTRIBUTE")]
        public new string Value { get; set; }
    }
}