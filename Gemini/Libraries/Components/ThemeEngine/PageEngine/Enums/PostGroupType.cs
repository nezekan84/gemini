﻿using System.ComponentModel.DataAnnotations;

namespace Gemini.Libraries.Components.ThemeEngine.PageEngine.Enums
{
    /// <summary>
    /// Since: Rev 26a
    /// Current: Rev 4
    /// </summary>
    public enum PostGroupType : byte
    {
        [Display(Name = "Category")]
        Category = 0,

        [Display(Name = "Tag")]
        Tag = 1
    }
}