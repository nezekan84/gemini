﻿using System.Collections.Generic;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.Factory;
using Gemini.Libraries.Components.ThemeEngine.ViewModelContainer;

namespace Gemini.Libraries.Components.ThemeEngine
{
    /// <summary>
    /// This class will contain the current states needed for BaseThemeServiceEngine to run.
    /// 
    /// Since: Rev 14a
    /// Current: Rev 5
    /// </summary>
    public class EngineState
    {
        /// <summary>
        /// disabled to prevent instantiation
        /// </summary>
        protected EngineState() { }

        /// <summary>
        /// Instance
        /// </summary>
        private static EngineState _instance;

        /// <summary>
        /// Get singleton instance
        /// </summary>
        public static EngineState Instance { get; } = _instance ?? (_instance = new EngineState());

        /// <summary>
        /// Current theme name.
        /// </summary>
        public string ActiveTheme;

        /// <summary>
        /// Qualified theme name / Service Provider class name.
        /// </summary>
        public string ServiceProviderName;

        /// <summary>
        /// Current service provider instance
        /// </summary>
        public BaseThemeServiceProvider ServiceProvider;

        /// <summary>
        /// Service provider collection
        /// </summary>
        public SortedDictionary<string, BaseThemeServiceProvider> ServiceProviders { get; } = new SortedDictionary<string, BaseThemeServiceProvider>();

        /// <summary>
        /// Factory instance. used for recreating view models.
        /// </summary>
        public ViewModelFactory ViewModelFactory { get;  } = new ViewModelFactory();

        /// <summary>
        /// ViewModelStates instance. every time a view model is modified, the engine will know.
        /// </summary>
        public ViewModelStates ViewModelStates { get; } = new ViewModelStates();
    }
}