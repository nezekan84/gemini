﻿using Gemini.Libraries.Components.ThemeEngine;
using Gemini.Themes.Classic.ViewModels.Pages;

namespace Gemini.Themes.Classic
{
    public class ClassicThemeServiceProvider : BaseThemeServiceProvider
    {
        public override void Boot()
        {
            
        }

        public override void RegisterViewModels()
        {
            ViewModels.Page = new ClassicPageViewModel();
        }

        public override void RegisterViews()
        {
            
        }
    }
}