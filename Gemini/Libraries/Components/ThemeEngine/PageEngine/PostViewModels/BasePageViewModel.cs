﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.Contracts;
using Gemini.Libraries.Components.ThemeEngine.PageEngine.Enums;
using Gemini.Libraries.Components.Toast;
using Gemini.Models;

namespace Gemini.Libraries.Components.ThemeEngine.PageEngine.PostViewModels
{
    /// <summary>
    /// Since: Rev 8a
    /// Current: Rev 4
    /// </summary>
    public class BasePageViewModel : IBasePostViewModel
    { 
        public int Id { get; set; }

        public string UserId { get; set; }

        public ApplicationUser User { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessage = "Title should not be empty.")]
        [StringLength(255, MinimumLength = 4, ErrorMessage = "Title should be between 4 - 255 characters long.")]
        public string Title { get; set; }

        [Display(Name = "Slug")]
        public string Slug { get; set; }

        [Display(Name = "Content")]
        public string Content { get; set; }

        public PostTypes Type
        {
            get { return PostTypes.Page; }
            set { }
        }

        [Display(Name = "Status")]
        public PostStatus Status { get; set; }

        public int ParentId { get; set; }

        [Display(Name = "Visibility")]
        public PostVisibility Visibility { get; set; }

        private List<BasePostMetaViewModel> _meta = new List<BasePostMetaViewModel>();

        public List<BasePostMetaViewModel> Meta
        {
            get { return _meta; }
            set { }
        }

        [Display(Name = "Date Updated")]
        public DateTime DateUpdated { get; set; }

        [Display(Name = "Date Published")]
        public DateTime DateCreated { get; set; }

        public List<RevisionViewModel> Revisions { get; set; }

        public string FormPartial { get; set; }

        public string ModelType
        {
            get { return GetType().FullName;  }
            set { }
        }

        public Notify Notify = new Notify();
    }
}

